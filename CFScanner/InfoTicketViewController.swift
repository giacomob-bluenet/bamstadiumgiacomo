//
//  InfoTickerViewController.swift
//  Steward Check-in
//
//  Created by Giacomo Boemio on 22/11/21.
//  Copyright © 2021 Bluenet srl. All rights reserved.
//

import Foundation
import UIKit

class InfoTicketViewController: UITableViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var steward: Steward?
    var modeType : ModeType?
    var event : Int?
    var codeExtra : String = ""
    var visibleBtnCheckIn : Bool = false


    var section: [String] = [NSLocalizedString("name", comment: ""),NSLocalizedString("surname", comment: ""),NSLocalizedString("date_of_birth", comment: ""),NSLocalizedString("cod_ticket", comment: ""),NSLocalizedString("city", comment: ""),NSLocalizedString("sector", comment: ""),NSLocalizedString("row", comment: ""),NSLocalizedString("seat", comment: ""),NSLocalizedString("check_in", comment: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Ticket Info", comment: "")
        if (visibleBtnCheckIn){
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Check in", style: .plain, target: self, action: #selector(checkOnClick))
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.section.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.section[section]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ticketIdentifier", for: indexPath)
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        cell.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.textLabel?.text = steward?.givenName
            }
        }
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                cell.textLabel?.text = steward?.familyName
            }
        }
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                cell.textLabel?.text = ""
                if  steward!.dob != nil {
                    cell.textLabel?.text = formatter.string(from: steward!.dob!)
                }
            }
        }
        if indexPath.section == 3 {
            if indexPath.row == 0 {
                cell.textLabel?.text = steward?.cf
            }
        }
        if indexPath.section == 4 {
            if indexPath.row == 0 {
                cell.textLabel?.text = steward?.city
            }
        }
        if indexPath.section == 5 {
            if indexPath.row == 0 {
                cell.textLabel?.text = steward?.sector
            }
        }
        if indexPath.section == 6 {
            if indexPath.row == 0 {
                cell.textLabel?.text = steward?.row
            }
        }
        if indexPath.section == 7 {
            if indexPath.row == 0 {
                cell.textLabel?.text = steward?.seat
            }
        }
        if indexPath.section == 8 {
            if indexPath.row == 0 {
                if let checkTime = steward?.checkTime {
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
                    df.timeZone = TimeZone.current
                    df.locale = Locale.current
                    df.dateStyle = .medium
                    df.timeStyle = .medium
                    let checkTimeFinal = df.string(from: checkTime)
                    cell.textLabel?.text = checkTimeFinal
                }else{
                    cell.textLabel?.text = ""
                }
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.darkGray
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
    
    @objc func checkOnClick() {
        switch steward!.extra {
            case "warn":
            print("yellow")
            case "cu":
            print("orange")
            default:
            self.codeExtra = steward!.extra ?? ""
        }
        
        let checkTime = Date.init()
        if(modeType == .checkIn) {
            steward!.checkTime = checkTime
            steward!.checkIn = CheckIn.local.rawValue
        } else {
            steward!.checkOutTime = checkTime
            steward!.checkOut = CheckOut.local.rawValue
        }
        if let user = self.steward, self.codeExtra != user.extra {
            DataManager.shared.addLog(forEvent: event!, cf: user.cf! , withTimestamp: checkTime,andExtra: self.codeExtra, andStatus: modeType == .checkIn ? "OK" : "OUT")
            user.extra = self.codeExtra
        } else {
            DataManager.shared.addLog(forEvent: event!, cf: steward!.cf!, withTimestamp: checkTime, andStatus: self.modeType == .checkIn ? "OK" : "OUT")
        }
        
        DispatchQueue.delay(.milliseconds(300)) { [self] in
            DataManager.shared.update(steward!)
        }
        
        self.navigationController?.popViewController(animated: true)
    }

}



class InfoTicketTableViewCell: UITableViewCell {
    @IBOutlet weak var lblValue: UILabel!
}
