//
//  StewardCreateViewController.swift
//  Steward Check-in
//
//  Created by Youssef on 02/10/2019.
//  Copyright © 2019 KM, Abhilash. All rights reserved.
//

import UIKit

struct City: Decodable{
    let cm: String
    let pv: String
}

class StewardCreateViewController: UIViewController {
    
    // MARK: - Initial Variebles
    let datePicker = UIDatePicker()
    var activeTxtField: UITextField?
    private var activeTxtView: UITextView?
    var event : Int?
    var steward: Steward?
    private var constraintButton : NSLayoutConstraint?
    var modeType : ModeType?
    var reset : Bool?
    
    // MARK: - IBOutlet Variebles

    @IBOutlet weak var sectorLabel: UILabel!
    @IBOutlet weak var scLabel: UILabel!
    
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var surnameTxtField: UITextField!
    @IBOutlet weak var dobTxtField: UITextField!
    @IBOutlet weak var cfTxtField: UITextField!
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var rowTxtField: UITextField!
    @IBOutlet weak var seatTxtField: UITextField!
    @IBOutlet weak var scTxtField: UITextField!
    @IBOutlet weak var sectorTxtField: UITextView!
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var scrollVIew: UIScrollView!
    @IBOutlet weak var contView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setConstrains()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        constraintButton = NSLayoutConstraint (item: cityTxtField,
                                                attribute: NSLayoutConstraint.Attribute.bottom,
                                                relatedBy: NSLayoutConstraint.Relation.equal,
                                                toItem: contView,
                                                attribute: NSLayoutConstraint.Attribute.bottom,
                                                multiplier: 1,
                                                constant: 2)
        setKeyboardToolbar()
        sectorTxtField.layer.borderWidth = 1
        sectorTxtField.layer.cornerRadius = 4
        if #available(iOS 13.0, *) {
            sectorTxtField.layer.borderColor = UIColor.systemGray4.cgColor
        } else {
            sectorTxtField.layer.borderColor = UIColor.systemGray.cgColor
        }
        //Check if user select steward in Steward list view controller
        if let steward = steward {
            
            // if user select a row the view will display info about the selected steward
            nameTxtField.isEnabled = false
            surnameTxtField.isEnabled = false
            dobTxtField.isEnabled = false
            cfTxtField.isEnabled = false
            cityTxtField.isEnabled = false
            rowTxtField.isEnabled = false
            seatTxtField.isEnabled = false
            sectorTxtField.isEditable = false
            sectorTxtField.isSelectable = false
            if #available(iOS 13.0, *) {
                sectorTxtField.backgroundColor = UIColor.systemGray6
            }
            scTxtField.isEnabled = modeType == .checkIn && steward.checkIn == CheckIn.noCheck.rawValue && steward.area == "LAZIO"
            
            nameTxtField.text = steward.givenName
            surnameTxtField.text = steward.familyName
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            if  steward.dob != nil {
                dobTxtField.text =  formatter.string(from: steward.dob!)
            }
            cfTxtField.text = steward.cf
            cityTxtField.text = steward.city
            rowTxtField.text = steward.row
            seatTxtField.text = steward.seat
            sectorTxtField.text = steward.sector

            switch steward.extra {
                case "warn":
                    self.view.backgroundColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
                case "cu":
                    self.view.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
                default:
                    self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    scTxtField.text = steward.extra
            }
            
            // Set Navigation Bar Item
            //navigationItem.title = NSLocalizedString("StewardInfo", comment: "")
            navigationItem.title = NSLocalizedString("Ticket Info", comment: "")
            
            setButton(createSteward: false)
            
        }else{
            // Else the view will allow the creation of new steward
            setButton(createSteward: true)
            
            // Set Dob Keyboard
            showDatePicker()
            
            // Listen for keyboard events
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
            
            // Set Navigation Bar Item
            navigationItem.title = NSLocalizedString("StewardCreations", comment: "")
            navigationItem.rightBarButtonItem = .init(barButtonSystemItem: .save, target: self, action: #selector(saveSteward))
        }
    }
        

    deinit {
            // Stop listening for keyboard hide/show events
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowCity") {
            
            if let CityViewController = segue.destination as? CityViewController, let Cities = self.getCityFromFile() {
                CityViewController.Cities = Cities
                CityViewController.delegate = self
            }
        }
    }

    // MARK: - Dob Keyboard Settings
    func showDatePicker(){
       // Formate Date
       datePicker.datePickerMode = .date

      // ToolBar
      let toolbar = UIToolbar();
      toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .done, target: self, action: #selector(doneDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)

     dobTxtField.inputAccessoryView = toolbar
     dobTxtField.inputView = datePicker

    }

     @objc func doneDatePicker(){

      let formatter = DateFormatter()
      formatter.dateFormat = "dd/MM/yyyy"
      dobTxtField.text = formatter.string(from: datePicker.date)
      self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
        
        // Dismiss the Keyboard
        self.view.endEditing(true)
     }
    
    
    // MARK: - City JSON Decoder methods

    func getCityFromFile() -> [City]? {
        
        // Get json path
        let url = Bundle.main.url(forResource: "comuni", withExtension: "json")!
        let data : Data = try! Data(contentsOf: url)
        var city: [City]?
        

        do {
            
            // Create city array
            let decoder: JSONDecoder = JSONDecoder.init()
            city = try decoder.decode([City].self, from: data)
    
        } catch let e {
            print(e)
        }
        
        return city
    }
    private func setButton(createSteward: Bool) {
        if(reset ?? false || createSteward ) {
            // Catch if cityBottom exist and change the constraint of city field
            if let constraint = contView.constraintWithIdentifier("cityBottom") {
                contView.removeConstraint(constraint)
                contView.addConstraint(constraintButton!)
            }
            
           // Remove check button
           checkButton.isHidden = true
           checkButton.isEnabled = false
        } else {
            // Set original constraints
            contView.removeConstraint(constraintButton!)
            contView.addConstraint(contView.constraintWithIdentifier("cityBottom")!)
            
            // Remove check button
            checkButton.isHidden = false
            checkButton.isEnabled = true
            
            // Set check button visul attribute
            checkButton.backgroundColor = modeType == .checkIn ? .systemBlue : .systemRed
            checkButton.layer.cornerRadius = 3
            checkButton.setTitle(modeType == .checkIn ? "Check-In" : "Check-Out", for: .normal)
            checkButton.tintColor = .white
            checkButton.titleLabel?.font = .boldSystemFont(ofSize: 25)
        }
    }
    
    @IBAction func checkOnClick(_ sender: Any) {
        let checkTime = Date.init()
        if(modeType == .checkIn) {
            steward!.checkTime = checkTime
            steward!.checkIn = CheckIn.local.rawValue
        } else {
            steward!.checkOutTime = checkTime
            steward!.checkOut = CheckOut.local.rawValue
        }
        if let user = self.steward, let newCode = scTxtField.text, newCode != user.extra {
            DataManager.shared.addLog(forEvent: event!, cf: user.cf! , withTimestamp: checkTime,andExtra: newCode, andStatus: modeType == .checkIn ? "OK" : "OUT")
            user.extra = newCode
        } else {
            DataManager.shared.addLog(forEvent: event!, cf: steward!.cf!, withTimestamp: checkTime, andStatus: self.modeType == .checkIn ? "OK" : "OUT")
        }
        
        DataManager.shared.update(steward!)
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - UITextFieldDelegate
extension StewardCreateViewController: UITextFieldDelegate {
    
    
    // MARK: - UITextFieldDelegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == cityTxtField){
            textField.resignFirstResponder()
            
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // Get TextField that generate the event
        activeTxtField = textField
        if(textField == cityTxtField){
            self.view.endEditing(true)
            self.performSegue(withIdentifier: "ShowCity", sender: nil)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Keyboard notification methods
    @objc func keyboardWillShow(notification: Notification) {
        
        // Get keyboard Size
        let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        
        // if the Active TextField is under the keyboard, the view will move up
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: {
            guard let textfield = self.activeTxtField else {return}
            let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + textfield.frame.height + 50, right: 0)
            self.scrollVIew.contentInset = contentInsets
            self.scrollVIew.scrollIndicatorInsets = contentInsets
        }, completion: nil)

    }

    @objc func keyboardWillHide(notification: Notification) {
        
        // Animation to set the view in original position
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: {
            self.scrollVIew.contentInset = .zero
            self.scrollVIew.scrollIndicatorInsets = .zero
        }, completion: nil)
    }
    
    // MARK: - NavigationController methods
    @objc func saveSteward() {
        // Check if all field are compiled
        print(self.datePicker.date)
        if (validateField()) {
            
            // Save Steward if all field are compiled and cf is correct
            let context = DataManager.shared.persistentContainer.viewContext
            let steward = Steward(context: context)
            steward.cf  = self.cfTxtField.text?.uppercased()
            steward.event = Int32(self.event ?? 0)
            steward.givenName = self.nameTxtField.text
            steward.familyName = self.surnameTxtField.text
            steward.dob = self.datePicker.date
            steward.city = self.cityTxtField.text
            steward.valid = true
            steward.sync = false
            steward.extra = self.scTxtField.text == "" ? nil : self.scTxtField.text
            steward.sector = self.sectorTxtField.text == "" ? nil : self.sectorTxtField.text
            steward.area = "STW"

            do {
                try context.save()
                navigationController?.popViewController(animated: true)
            } catch {
                let alert = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: NSLocalizedString("alertErrorAdd", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("alertButton", comment: ""), style: .default, handler: nil))
                self.present(alert, animated:true, completion: nil)
                context.rollback()
            }
        }
    }
}

extension StewardCreateViewController {
    
    // MARK: - CheckCf method

    private func checkCf(cf : String) -> Bool {
        
        // Check cf number of characters
        if(cf.count != 16) {
            let alert = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "La lunghezza del Codice non è corretta. Deve essere 16 caratteri.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("alertButton", comment: ""), style: .default, handler: nil))
            self.present(alert, animated:true, completion: nil)
            return false
        }
        
        // Check if the first part(name,surname) are letters
        var lowerBound = cf.index(cf.startIndex, offsetBy: 0)
        var upperBound = cf.index(cf.startIndex, offsetBy: 6)
        if (cf[lowerBound..<upperBound].rangeOfCharacter(from: CharacterSet.decimalDigits) != nil) {
            return false
        }
        
        // Check if the part(Yob) are decimalDigits
        lowerBound = cf.index(cf.startIndex, offsetBy: 6)
        upperBound = cf.index(cf.startIndex, offsetBy: 8)
        if (cf[lowerBound..<upperBound].rangeOfCharacter(from: CharacterSet.letters) != nil) {
            return false
        }
        
        // Check if the part(gender) are letters
        lowerBound = cf.index(cf.startIndex, offsetBy: 8)
        upperBound = cf.index(cf.startIndex, offsetBy: 9)
        if (cf[lowerBound..<upperBound].rangeOfCharacter(from: CharacterSet.decimalDigits) != nil) {
            return false
        }
        
        // Check if the part(dob) are decimalDigits
        lowerBound = cf.index(cf.startIndex, offsetBy: 9)
        upperBound = cf.index(cf.startIndex, offsetBy: 11)
        if (cf[lowerBound..<upperBound].rangeOfCharacter(from: CharacterSet.letters) != nil) {
            return false
        }
        
        // Check if part(City) are alphanumerics
        lowerBound = cf.index(cf.startIndex, offsetBy: 11)
        upperBound = cf.index(cf.startIndex, offsetBy: 15)
        if (cf[lowerBound..<upperBound].rangeOfCharacter(from: CharacterSet.alphanumerics) == nil) {
            return false
        }
        
        // Check if the last character are letters
        lowerBound = cf.index(cf.startIndex, offsetBy: 15)
        upperBound = cf.index(cf.startIndex, offsetBy: 16)
        if (cf[lowerBound..<upperBound].rangeOfCharacter(from: CharacterSet.decimalDigits) != nil) {
            return false
        }
        
        return true
    }
    
    // Check if all field are compiled and cf is correct
    private func validateField() -> Bool {
        
        if(self.nameTxtField.text!.isEmpty || self.surnameTxtField.text!.isEmpty || self.dobTxtField.text!.isEmpty || self.cfTxtField.text!.isEmpty || self.cityTxtField.text!.isEmpty ){
               // Show alert if one of them is Empty
               let alert = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: NSLocalizedString("alertMessage", comment: ""), preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: NSLocalizedString("alertButton", comment: ""), style: .default, handler: nil))
               self.present(alert, animated:true, completion: nil)
            
               return false
        } else if !(checkCf(cf: cfTxtField.text!)) {
            
            let alert = UIAlertController(title: NSLocalizedString("alertTitle", comment: ""), message: "Il Codice Fiscale non è corretto.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("alertButton", comment: ""), style: .default, handler: nil))
            self.present(alert, animated:true, completion: nil)
            
            return false
        }
        
        return true
    }
        
}

// MARK: - CityViewControllerDelegate method


extension StewardCreateViewController: CityViewControllerDelegate {
    
    func CityViewControllerResponse(city: String) {
        
        // Get results from the city view controller
        cityTxtField.text = city
    }

}

extension UIView {
    /// Returns the first constraint with the given identifier, if available.
    ///
    /// - Parameter identifier: The constraint identifier.
    func constraintWithIdentifier(_ identifier: String) -> NSLayoutConstraint? {
        return self.constraints.first { $0.identifier == identifier }
    }
}

extension StewardCreateViewController {
    
    func setKeyboardToolbar() {
        let numberToolbar: UIToolbar = UIToolbar()
        
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: UIBarButtonItem.Style.done, target: self, action: #selector(apply))
        ]

        numberToolbar.sizeToFit()

        scTxtField.inputAccessoryView = numberToolbar
    }


    @objc private func apply () {
        //do somethings
        scTxtField.resignFirstResponder()
    }
    
}
