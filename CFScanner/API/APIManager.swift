//
//  DataController.swift
//  CFScanner
//
//  Created by Alessandro de Peppo on 19/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//

import UIKit
import CoreData

struct Settings {
//    static let host = "bluenetita.info"
//    static let port : Int? = nil
//    static let scheme = "https"
//    static let path = "/api/t1"
//    static let credentials = Credentials(username: "s5cn.Mango-riot", password: "exert521)sAturate")
    
    
//    //test jar funzionante
    /*static let host = "192.168.1.28"
    static let port : Int? = 9021
    static let scheme = "http"
    static let path = ""
    static var credentials = Credentials(username: "", password: "")*/
    
    //dev jar funzionante
//    static let host = "93.186.255.152"
//    static let port : Int? = 9020
//    static let scheme = "http"
//    static let path = ""
//    static let credentials = Credentials(username: "s5cn.Mango-riot", password: "exert521)sAturate")
    
    //prod war
        static let host = "93.186.255.152"
        static let port : Int? = 9248
        static let scheme = "http"
        static let path = "/bam-ticket"
        //static let credentials = Credentials(username: "s5cn.Mango-riot", password: "exert521)sAturate")
        static let credentials = Credentials(username: "", password: "")

    
}

class APIManager {
    
    static let shared = APIManager(credentials: Settings.credentials )
    
    var base64Credentials : String?
    var base64CredentialsTwo : String?
    let semaphore = DispatchSemaphore(value: 1)
    
    init(credentials : Credentials ) {
        
        let loginString = "\(credentials.username):\(credentials.password)"
        guard let loginData = loginString.data(using: String.Encoding.utf8) else {
            self.base64Credentials = nil
            return
        }
        self.base64Credentials = loginData.base64EncodedString()
    }
    
    @objc func loadEvents(onSuccess: @escaping(Bool) -> Void, onFailure: @escaping(String) -> Void){
        print("fetching Events")
        //Acquires a permit from this semaphore
        //semaphore.wait()
        
        let endpoint = Endpoint(path: "/events", queryItems: nil)
        if let url = endpoint.url {
            print("fetching data from: \(url.absoluteString)")
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            if let creds = self.base64Credentials {
                request.setValue("Basic \(creds)", forHTTPHeaderField: "Authorization")
            }
            request.timeoutInterval = 15
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if error != nil {
                    print("error=\(error!)")
                    //Release the semaphore when there is no connection or other errors
                    //self.semaphore.signal()
                    DispatchQueue.main.async {
                        onFailure("error_server")
                    }
                    return
                }
                guard let httpResponse = response as? HTTPURLResponse,
                      (200...299).contains(httpResponse.statusCode) else {
                    print("Error Server loadevents")
                    //Releases a permit, returning it to the semaphore.
                    //self.semaphore.signal()
                    DispatchQueue.main.async {
                        onFailure("error_server")
                    }
                    return
                }
                
                if let data = data {
                    do {
                        let events = try JSONDecoder().decode([EventsApi].self, from: data)
                        
                        // The context automatically merges changes to its parent context. ( So you can get the notification )
                        DataManager.shared.persistentContainer.performBackgroundTask{(context) in
                            for item in events {
                                if DataManager.shared.getEventWithContext(context, forEvent: item.id) != nil {
                                    print("loadEvents - WARN: Cannot update an existing steward \(item.id )")
                                } else {
                                    print("New event entry with code \(item.id)")
                                    let event = Event(context: context)
                                    event.id = Int64(item.id)
                                    event.name = item.name
                                    if let startTime = item.startDate {
                                        event.startTime = DateFormatter.iso8601UTC.date(from: startTime)
                                    }
                                }
                            }
                            do {
                                try context.save()
                                DispatchQueue.main.async {
                                    onSuccess(true)
                                }
                                //Releases a permit, returning it to the semaphore.
                                //self.semaphore.signal()
                            } catch {
                                print("loadEvents - Failure to save context: \(error)")
                                DispatchQueue.main.async {
                                    onFailure("error_context")
                                }
                                context.rollback()
                                //Releases a permit, returning it to the semaphore.
                                //self.semaphore.signal()
                            }
                        }
                    } catch {
                        print(error)
                        //Releases a permit, returning it to the semaphore.
                        //self.semaphore.signal()
                        DispatchQueue.main.async {
                            onFailure("error_server")
                        }
                    }
                }
            }
            task.resume()
        } else {
            print("ERROR: unable to create endpoint url")
            //Releases a permit, returning it to the semaphore.
            //semaphore.signal()
            DispatchQueue.main.async {
                onFailure("error_unknown")
            }
        }
    }
    
    
    @objc func loadStewards(forEvent event: Int, onSuccess: @escaping(Bool) -> Void, onFailure: @escaping(String) -> Void){
        print("fetching stewards start")
        var lastCode = UserDefaults.standard.string(forKey: "lastCode.\(event)") ?? "0"
        var stringAreas = ""
        var stringSectors = ""

        DispatchQueue.main.async { [self] in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if(appDelegate.arrAreasAndSectorsFinal.count > 0){
                for (key, value) in appDelegate.arrAreasAndSectorsFinal{
                    if(stringAreas.isEmpty){
                        stringAreas = key
                    }else{
                        stringAreas = stringAreas + "," + key
                    }
                    if(stringSectors.isEmpty){
                        stringSectors = value.joined(separator: ",")
                    }else{
                        stringSectors = stringSectors + "," + value.joined(separator: ",")
                    }
                }
            }
            
            let endpoint = Endpoint(path: "/events/\(event)/tickets", queryItems:
                [URLQueryItem(name: "lastCode", value: "\(lastCode)"),
                 URLQueryItem(name: "area", value: stringAreas),
                 URLQueryItem(name: "sector", value: stringSectors),
                 URLQueryItem(name: "deviceId", value: "\(UIDevice.current.name)")])

            //Acquires a permit from this semaphore
            semaphore.wait()
            if let url = endpoint.url {
                print("fetching data from: \(url.absoluteString)")
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                if let creds = self.base64Credentials {
                    request.setValue("Basic \(creds)", forHTTPHeaderField: "Authorization")
                }
                request.timeoutInterval = 15
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    if error != nil {
                        print("error=\(error!)")
                        //Release the semaphore when there is no connection or other errors
                        self.semaphore.signal()
                        DispatchQueue.main.async {
                            onFailure("error_server")
                        }
                        return
                    }
                    
                    guard let httpResponse = response as? HTTPURLResponse,
                          (200...299).contains(httpResponse.statusCode) else {
                        print("Error Server loadStewards")

                        //Releases a permit, returning it to the semaphore.
                        self.semaphore.signal()
                        DispatchQueue.main.async {
                            onFailure("error_server")
                        }
                        return
                    }
                    
                    if let data = data {
                        do {
                            let tickets = try JSONDecoder().decode([TicketApi].self, from: data)
                            // The context automatically merges changes to its parent context. ( So you can get the notification )
                            DataManager.shared.persistentContainer.performBackgroundTask{(context) in
                                
                                for item in tickets {
                                    if DataManager.shared.getStewardWithContext(context,  item.code, forEvent: event) != nil {
                                            print("loadStewards - WARN: Cannot update an existing steward \(item.code)")
                                    } else {
                                        print("loadStewards - New Ticket entry with code \(item.code)")
                                        let steward = Steward(context: context)
                                        //let extra = JSONDecoder().decode([ExtraApi].self, from: item.extra)
                                        steward.cf = item.code
                                        steward.event = Int32(event)
                                        steward.checkIn = item.used ?  CheckIn.external.rawValue : CheckIn.noCheck.rawValue
                                        if let dob = item.dob {
                                            steward.dob = DateFormatter.dfDOB.date(from: dob)
                                        }
                                        if let area = item.area {
                                            steward.area = area
                                        }
                                        steward.familyName = item.familyName
                                        steward.givenName = item.givenName
                                        steward.city = item.pob
                                        steward.sector = item.sector
                                        steward.valid = item.valid
                                        steward.extra = item.extra
                                        steward.row = item.row
                                        steward.seat = item.seat

                                        if let ts = item.ts {
                                            let checkTime = DateFormatter.dfTS.date(from: ts)
                                            steward.checkTime = checkTime
                                        }
                                    }
                                    lastCode = item.code
                                }
                                
                                do {
                                    UserDefaults.standard.set(lastCode, forKey: "lastCode.\(event)")
                                    try context.save()
                                    DispatchQueue.main.async {
                                        onSuccess(true)
                                    }
                                    //Releases a permit, returning it to the semaphore.
                                    self.semaphore.signal()
                                } catch {
                                    print("loadStewards - Failure to save context: \(error)")
                                    DispatchQueue.main.async {
                                        onFailure("error_context")
                                    }
                                    context.rollback()
                                    //Releases a permit, returning it to the semaphore.
                                    self.semaphore.signal()
                                }
                                
                            }
                            
                        } catch {
                            print(error)
                            //Releases a permit, returning it to the semaphore.
                            self.semaphore.signal()
                            DispatchQueue.main.async {
                                onFailure("error_server")
                            }
                        }
                    }
                }
                task.resume()
            } else {
                print("ERROR: unable to create endpoint url")
                //Releases a permit, returning it to the semaphore.
                self.semaphore.signal()
                DispatchQueue.main.async {
                    onFailure("error_unknown")
                }
            }

        }
        
        

    }
    
    @objc func addStewards(forEvent event: Int, onSuccess: @escaping(Bool) -> Void, onFailure: @escaping(String) -> Void){
        print("API add stewards")
        DataManager.shared.persistentContainer.performBackgroundTask{(context) in
            guard let stewards = DataManager.shared.fetchNewStewards(forEvent: event, withContext: context), stewards.count > 0 else {
                print("No Steward to be synced")
                DispatchQueue.main.async {
                    onSuccess(true)
                }
                return
            }
            
            for steward in stewards {
                let ts : String? = steward.checkTime != nil ? DateFormatter.dfTS.string(from: steward.checkTime!) : nil
                let dob : String? = steward.dob != nil ? DateFormatter.dfDOB.string(from: steward.dob!) : nil
                
                /*print(ts)
                print(dob)
                print("ts + dob")*/

                let apiSteward = TicketApi(
                    code: steward.cf!,
                    ts: ts,
                    cardType: "A",
                    area: steward.area,
                    sector: steward.sector,
                    used: steward.checkIn >= CheckIn.local.rawValue ? true : false,
                    valid: steward.valid,
                    givenName: steward.givenName,
                    familyName: steward.familyName,
                    dob: dob,
                    pob: steward.city,
                    extra: steward.extra,
                    row: steward.row,
                    seat: steward.seat
                    )
                
                let list = [apiSteward]
                let jsonData = try! JSONEncoder().encode(list)
                
                let endpoint = Endpoint(path: "/events/\(event)/tickets",
                    queryItems: nil)

                if let url = endpoint.url {
                    print("posting data to: \(url.absoluteString)")
                    var request = URLRequest(url: url)
                    request.httpMethod = "POST"
                    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON
                    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
                    if let creds = self.base64Credentials {
                        request.setValue("Basic \(creds)", forHTTPHeaderField: "Authorization")
                    }
                    request.timeoutInterval = 15
                    request.httpBody = jsonData
                    
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        if error != nil {
                            print("error=\(error!)")
                            DispatchQueue.main.async {
                                onFailure("error_server")
                            }
                            return
                        }
                        
                        guard let data = data,
                            let response = response as? HTTPURLResponse,
                            error == nil else {                                              // check for fundamental networking error
                                print("error", error ?? "Unknown error")
                                DispatchQueue.main.async {
                                    onFailure("error_server")
                                }
                                return
                        }
                        
                        guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                            print("statusCode should be 2xx, but is \(response.statusCode)")
                            print("response = \(response)")
                            DispatchQueue.main.async {
                                onFailure("error_server")
                            }
                            return
                        }
                        
                        /*do {
                            let json = try JSONSerialization.jsonObject(with: data)
                            print(json)
                            print("json addSteward")
                            }
                        catch {
                            print("Could not convert JSON data into a dictionary.")
                        }*/
                        
                        do {
                            let decoder = JSONDecoder()
                            /*let result = try decoder.decode(ResponseApi.self, from: data)
                            if let code = steward.cf, result.id == code {
                                steward.sync = true
                                do {
                                    try context.save()
                                    DispatchQueue.main.async {
                                        onSuccess(true)
                                    }
                                } catch {
                                    print("addStewards - Failure to save context: \(error)")
                                    context.rollback()
                                    DispatchQueue.main.async {
                                        onFailure("error_context")
                                    }
                                }
                            }*/
                            let result = try decoder.decode([String].self, from: data)
                            if (result.count > 0){
                                for cf in result {
                                    if let code = steward.cf, cf == code {
                                        steward.sync = true
                                        do {
                                            try context.save()
                                            DispatchQueue.main.async {
                                                onSuccess(true)
                                            }
                                        } catch {
                                            print("addStewards - Failure to save context: \(error)")
                                            context.rollback()
                                            DispatchQueue.main.async {
                                                onFailure("error_context")
                                            }
                                        }
                                    }
                                }
                            }else{
                                print("no AddStewards")
                                DispatchQueue.main.async {
                                    onSuccess(true)
                                }
                            }
                        } catch {
                            print("Add Stewards ERROR: \(error)")
                            DispatchQueue.main.async {
                                onFailure("error_server")
                            }
                        }
                        
                    }
                    task.resume()
                } else {
                    print("error: unable to create endpoint url")
                    DispatchQueue.main.async {
                        onFailure("error_unknown")
                    }
                }
            }
        }
    }
    
    @objc func addLogs(forEvent event: Int, onSuccess: @escaping(Bool) -> Void, onFailure: @escaping(String) -> Void){
        //File download
        print("add logs")
        DataManager.shared.persistentContainer.performBackgroundTask{(context) in
            guard let logs = DataManager.shared.fetchLogs(forEvent: event, withContext: context), logs.count > 0 else {
                print("No Logs to be synced")
                DispatchQueue.main.async {
                    onSuccess(true)
                }
                return
            }

            let apiLogs = logs.map{ LogApi(id: nil,
                                           code: $0.cf!,
                                           type: "A",
                                           codeType: "BR",
                                           extra: $0.extra,
                                           status: $0.status!,
                                           ts: $0.timestamp!)}
            
            /*print("timestamp ts")
            print(apiLogs[0].ts)*/

            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .formatted(.dfTS)
            let jsonData = try! encoder.encode(apiLogs)
            var stringAreas = ""
            var stringSectors = ""
            
            DispatchQueue.main.async {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if(appDelegate.arrAreasAndSectorsFinal.count > 0){
                    for (key, value) in appDelegate.arrAreasAndSectorsFinal{
                        if(stringAreas.isEmpty){
                            stringAreas = key
                        }else{
                            stringAreas = stringAreas + "," + key
                        }
                        if(stringSectors.isEmpty){
                            stringSectors = value.joined(separator: ",")
                        }else{
                            stringSectors = stringSectors + "," + value.joined(separator: ",")
                        }
                    }
                }
                
                let endpoint = Endpoint(path: "/events/\(event)/logs", queryItems:
                     [URLQueryItem(name: "area", value: stringAreas),
                     URLQueryItem(name: "sector", value: stringSectors),
                     URLQueryItem(name: "deviceId", value: "\(UIDevice.current.name)")])
                
                /*let endpoint = Endpoint(path: "/events/\(event)/logs",
                    queryItems: [URLQueryItem(name: "deviceId", value: "\(UIDevice.current.name)")])*/
                
                if let url = endpoint.url {
                    print("posting data to: \(url.absoluteString)")
                    var request = URLRequest(url: url)
                    request.httpMethod = "POST"
                    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON
                    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
                    if let creds = self.base64Credentials {
                        request.setValue("Basic \(creds)", forHTTPHeaderField: "Authorization")
                    }
                    request.timeoutInterval = 15
                    request.httpBody = jsonData
                    
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        if error != nil {
                            print("error=\(error!)")
                            DispatchQueue.main.async {
                                onFailure("error_server")
                            }
                            return
                        }
                        
                        guard let data = data,
                            let response = response as? HTTPURLResponse,
                            error == nil else {                                              // check for fundamental networking error
                                print("error", error ?? "Unknown error")
                                DispatchQueue.main.async {
                                    onFailure("error_server")
                                }
                                return
                        }
                        
                        guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                            print("statusCode should be 2xx, but is \(response.statusCode)")
                            print("response = \(response)")
                            DispatchQueue.main.async {
                                onFailure("error_server")
                            }
                            return
                        }
                        do {
                            let decoder = JSONDecoder()
                            let results = try decoder.decode([String].self, from: data)
                            for log in logs  {
                                if let code = log.cf, (results.contains(code)) {
                                    log.sync = true
                                }
                            }
                            do {
                                try context.save()
                                DispatchQueue.main.async {
                                    onSuccess(true)
                                }
                            } catch {
                                print("addLogs - Failure to save context: \(error)")
                                context.rollback()
                                DispatchQueue.main.async {
                                    onFailure("error_context")
                                }
                            }
                        } catch {
                            print(error)
                            DispatchQueue.main.async {
                                onFailure("error_server")
                            }
                        }
                        
                    }
                    task.resume()
                } else {
                    print("error: unable to create endpoint url")
                    DispatchQueue.main.async {
                        onFailure("error_unknown")
                    }
                }
            }
            
            }

            
            
        
    }
    
    @objc func loadLogs(forEvent event: Int, onSuccess: @escaping(Bool) -> Void, onFailure: @escaping(String) -> Void){
        print("fetching logs")
        var lastLog = UserDefaults.standard.integer(forKey: "lastLog")
        
        let endpoint = Endpoint(path: "/events/\(event)/logs", queryItems:
            [URLQueryItem(name: "logId", value: "\(lastLog)"),
             URLQueryItem(name: "deviceId", value: "\(UIDevice.current.name)")])
        if let url = endpoint.url {
            print("fetching data from: \(url.absoluteString)")
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            if let creds = self.base64Credentials {
                request.setValue("Basic \(creds)", forHTTPHeaderField: "Authorization")
            }
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if error != nil {
                    print("error=\(error!)")
                    DispatchQueue.main.async {
                        onFailure("error_server")
                    }
                    return
                }
                
                if let data = data {
                    do {
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .formatted(.iso8601UTC)
                        let tickets = try decoder.decode([LogApi].self, from: data)
                        if (tickets.count > 0) {
                            DataManager.shared.persistentContainer.performBackgroundTask{(context) in
                                for item in tickets {
                                    if let steward = DataManager.shared.getStewardWithContext(context,  item.code, forEvent: event)  {
                                        if (steward.checkIn == CheckIn.noCheck.rawValue && item.status == "OK") {
                                            //We have a checkin
                                            steward.checkTime = item.ts
                                            
                                            if let extra = item.extra {
                                                steward.extra = extra
                                            }
                                            steward.checkIn = CheckIn.external.rawValue
                                        } else if (item.status == "RESET") {
                                            //We have a Reset( Check-in Mode )
                                            steward.checkTime = nil
                                            steward.checkOutTime = nil
                                            steward.checkIn = CheckIn.noCheck.rawValue
                                            steward.checkOut = CheckOut.noCheck.rawValue
                                        } else if (item.status == "INVALID") {
                                                //We have a Invalidation
                                                steward.valid = false
                                        } else if (steward.checkOut == CheckOut.noCheck.rawValue && item.status == "OUT") {
                                            //We have a Checkout
                                            steward.checkOutTime = item.ts
                                            steward.checkOut = CheckOut.external.rawValue
                                        }
                                    } else {
                                        print("ERROR: Unable to find the steward with code: \(item.code)")
                                    }
                                    lastLog = item.id!
                                }
                                DispatchQueue.main.async {
                                    onSuccess(true)
                                }
                                do {
                                    UserDefaults.standard.set(lastLog, forKey: "lastLog")
                                    try context.save()
                                } catch {
                                    print("loadLogs - Failure to save context: \(error)")
                                    context.rollback()
                                    DispatchQueue.main.async {
                                        onFailure("error_context")
                                    }
                                }
                            }
                        }
                    } catch {
                        print(error)
                        DispatchQueue.main.async {
                            onFailure("error_server")
                        }
                    }
                }
            }
            task.resume()
        } else {
            print("error: unable to create endpoint url")
            DispatchQueue.main.async {
                onFailure("error_unknown")
            }
        }
    }
    
    func getAreas(event: String, onSuccess: @escaping([String]) -> Void, onFailure: @escaping(String) -> Void){
        print("fetch areas")
        let endpoint = Endpoint(path: "/events/\(event)/areas", queryItems:
            [URLQueryItem(name: "deviceId", value: "\(UIDevice.current.name)")])

        var request = URLRequest(url: endpoint.url!)
        request.httpMethod = "GET"
        if let creds = self.base64Credentials {
            request.setValue("Basic \(creds)", forHTTPHeaderField: "Authorization")
        }
            
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                print("error=\(error!)")
                DispatchQueue.main.async {
                    onFailure("error_server")
                }
                return
            }
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(.iso8601UTC)
                    let result = try decoder.decode([String].self, from: data)
                    onSuccess(result)
                } catch {
                    print(error.localizedDescription)
                    DispatchQueue.main.async {
                        onFailure("error_unknown")
                    }
                }
            }
        }
        task.resume()
     }
    
    func getSectors(event: String, area: String, onSuccess: @escaping([String]) -> Void, onFailure: @escaping(String) -> Void){
        print("fetch Sectors")
        
        let endpoint = Endpoint(path: "/events/\(event)/sectors", queryItems:
            [URLQueryItem(name: "area", value: "\(area)"),
             URLQueryItem(name: "deviceId", value: "\(UIDevice.current.name)")])
        print(endpoint.url!)
        print("endpoint sectors")

        var request = URLRequest(url: endpoint.url!)
        request.httpMethod = "GET"
        if let creds = self.base64Credentials {
            request.setValue("Basic \(creds)", forHTTPHeaderField: "Authorization")
        }
            
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                print("error=\(error!)")
                DispatchQueue.main.async {
                    onFailure("error_server")
                }
                return
            }
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(.iso8601UTC)
                    let result = try decoder.decode([String].self, from: data)
                    onSuccess(result)
                } catch {
                    print(error.localizedDescription)
                    DispatchQueue.main.async {
                        onFailure("error_unknown")
                    }
                }
            }
        }
        task.resume()
     }
    
    func loginSteward(username: String, password: String, onSuccess: @escaping(Bool) -> Void, onFailure: @escaping(String) -> Void){
        print("login App")
        
        let endpoint = Endpoint(path: "/login", queryItems: [])
        print(endpoint.url!)
        print("endpoint login")
        let base64CredentialsLocal : String?
        let loginString = "\(username):\(password)"
        //APIManager.shared = Credentials()

        guard let loginData = loginString.data(using: String.Encoding.utf8) else {
            base64CredentialsLocal = nil
            return
        }
        base64CredentialsLocal = loginData.base64EncodedString()

        var request = URLRequest(url: endpoint.url!)
        request.httpMethod = "POST"
        if let creds = base64CredentialsLocal {
            request.setValue("Basic \(creds)", forHTTPHeaderField: "Authorization")
        }
        request.timeoutInterval = 15
            
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if error != nil {
                print("error=\(error!)")
                DispatchQueue.main.async {
                    onFailure("error_server")
                }
                return
            }
            let res = response as! HTTPURLResponse?
            if ((res?.statusCode)! >= 200 && (res?.statusCode)! <= 500)
            {
                if(res?.statusCode == 200 || res?.statusCode == 201)
                {
                    if let data = data {
                        do {
                            let decoder = JSONDecoder()
                            decoder.dateDecodingStrategy = .formatted(.iso8601UTC)
                            let result = try decoder.decode(Bool.self, from: data)
                            onSuccess(result)
                        } catch {
                            print(error.localizedDescription)
                            onFailure("error_unknown")
                        }
                    }
                }else{
                    if ((res?.statusCode)! == 500){
                        DispatchQueue.main.async {
                            onFailure("error_internalserver")
                        }
                    }else if ((res?.statusCode)! == 501){
                        DispatchQueue.main.async {
                            onFailure("error_notimplemented")
                        }
                    }else if ((res?.statusCode)! == 400){
                        DispatchQueue.main.async {
                            onFailure("error_badrequest")
                        }
                    }else if ((res?.statusCode)! == 401){
                        DispatchQueue.main.async {
                            onFailure("error_unauthorized")
                        }
                    }else if ((res?.statusCode)! == 403){
                        DispatchQueue.main.async {
                            onFailure("error_forbidden")
                        }
                    }else if ((res?.statusCode)! == 404){
                        DispatchQueue.main.async {
                            onFailure("error_not_found")
                        }
                    }else if ((res?.statusCode)! == 405){
                        DispatchQueue.main.async {
                            onFailure("error_method_not_allowed")
                        }
                    }else if ((res?.statusCode)! == 406){
                        DispatchQueue.main.async {
                            onFailure("error_not_acceptable")
                        }
                    }else{
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                            print(jsonData as Any)
                            DispatchQueue.main.async {
                                onFailure("error_unknown")
                            }
                        } catch let error as NSError {
                            print(error.code)
                            DispatchQueue.main.async {
                                onFailure("error_server")
                            }
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                    onFailure("error_server")
                }
            }
        }
        task.resume()
     }
}

struct Credentials {
    let username : String
    let password : String
}


struct Endpoint {
    let path: String
    let queryItems: [URLQueryItem]?
}

extension Endpoint {
    // We still have to keep 'url' as an optional, since we're
    // dealing with dynamic components that could be invalid.
    var url: URL? {
        var components = URLComponents()
        components.scheme = Settings.scheme
        components.host = Settings.host
        if let port = Settings.port {
            components.port = port
        }
        components.path = "\(Settings.path)\(path)"
        components.queryItems = queryItems
        
        return components.url
    }
}

extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"

        return dateFormatter.string(from: Date())

    }
}
