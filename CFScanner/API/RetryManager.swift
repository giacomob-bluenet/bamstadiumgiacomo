//
//  Retry.swift
//
//  Created by Giacomo Boemio on 10/07/2020.
//  Copyright © 2020 Giacomo. All rights reserved.
//

import Foundation
import UIKit

class RetryManager: NSObject {
    
    static let sharedInstance = RetryManager()
    var currentCount = 0
    
    func startRetry(retryCount: Int, onResult: @escaping (Bool) -> Void) {
        if(currentCount < retryCount)
        {
            currentCount += 1
            onResult(true)
        }else{
            onResult(false)
        }
    }
}
