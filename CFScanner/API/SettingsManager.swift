//
//  SettingsManager.swift
//  CFScanner
//
//  Created by Giacomo Boemio on 26/10/21.
//  Copyright © 2021 Bluenet srl. All rights reserved.
//

import Foundation
import UIKit

class SettingsManager : NSObject{
    
    class var sharedInstance: SettingsManager {
        struct Static {
            static let instance: SettingsManager = SettingsManager()
        }
        return Static.instance
    }
    
    fileprivate var defaultStorage = UserDefaults.standard
    
    //save ModeCheck CAMERA or RING
    var saveModeCheck: String {
        get{
            if defaultStorage.object(forKey: "saveModeCheck") != nil {
                let returnValue = defaultStorage.object(forKey: "saveModeCheck") as! String
                return returnValue
            }else{
                return ""
            }
        }set{
            defaultStorage.set(newValue, forKey: "saveModeCheck")
        }
    }
    
    var saveArrArea: [String] {
        get{
            if defaultStorage.object(forKey: "saveaArrArea") != nil {
                let returnValue = defaultStorage.object(forKey: "saveaArrArea") as! [String]
                return returnValue
            }else{
                return []
            }
        }set{
            defaultStorage.set(newValue, forKey: "saveaArrArea")
        }
    }
    
}
