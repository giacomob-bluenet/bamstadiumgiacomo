//
//  NapoliContView.swift
//  Steward Check-in lazio
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import UIKit

extension StewardCreateViewController {
    
    func setConstrains() {
        sectorTxtField.isHidden = true
        scTxtField.isHidden = true
        sectorLabel.isHidden = true
        scLabel.isHidden = true
        let constraintTop = NSLayoutConstraint (item: nameTxtField,
                                                attribute: NSLayoutConstraint.Attribute.top,
                                                relatedBy: NSLayoutConstraint.Relation.equal,
                                                toItem: contView,
                                                attribute: NSLayoutConstraint.Attribute.top,
                                                multiplier: 1,
                                                constant: 45)
        if let constraint = contView.constraintWithIdentifier("nameField") {
            contView.removeConstraint(constraint)
            contView.addConstraint(constraintTop)
        }
    }
    
}
