//
//  NapoliDetailVIew.swift
//  Steward Check-in
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import UIKit

public class DetailView: UIView {
    @IBOutlet var DetailView: DetailView!
    @IBOutlet weak var confButton: UIButton!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var checked: UILabel!
    @IBOutlet weak var dob: UILabel!
    @IBOutlet weak var cf: UILabel!
    @IBOutlet weak var cognome: UILabel!
    @IBOutlet weak var row: UILabel!
    @IBOutlet weak var seat: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var lblCognome: UILabel!
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblFila: UILabel!
    @IBOutlet weak var lblPosto: UILabel!

    override init(frame: CGRect) {

        // Call super init

        super.init(frame: frame)

        // 3. Setup view from .xib file

        configureXIB(named: "NapoliDetailView")

    }

    required init?(coder aDecoder: NSCoder) {

        // 1. setup any properties here

        // 2. call super.init(coder:)

        super.init(coder: aDecoder)

        // 3. Setup view from .xib file

        configureXIB(named: "NapoliDetailView")

    }
    
}
