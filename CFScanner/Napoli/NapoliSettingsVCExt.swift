//
//  NapoliSettingsVCExt.swift
//  Steward Check-in
//
//  Created by Youssef on 05/02/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import UIKit

extension SettingsViewController {
    
    internal func setSection() -> [String] {
        return ["List","Export","ModeCheck","AreaSector"]
    }
    
    internal func saveSettings() {
        if(changeEvent){
            if  UserDefaults.standard.integer(forKey: "event") != currentEvent  {
                UserDefaults.standard.set(currentEvent, forKey: "event")
                changeEvent = false
            }
        }
    }
    
    func numberOfRowsInSection(withSection section: String) -> Int {
        switch section {
        case "Export":
            return 2
        case "List":
            print("Num Eventi: \(events?.count)")
            return events?.count ?? 0
            //return 70
        case "ModeCheck":
            return 2
        case "AreaSector":
            return self.arrAreas.count
        default:
            return 1
        }
    }
    
    internal func handleRowSelection(withSection section: String, withTableView tableView: UITableView, withIndexPath indexPath: IndexPath) {
        switch section {
           case "Export":
                handleExport(tableView: tableView, indexPath: indexPath)
           case "List":
                handleList(tableView: tableView, indexPath: indexPath)
           case "ModeCheck":
                handleModeCheck(tableView: tableView, indexPath: indexPath)
           case "AreaSector":
                handleArea(tableView: tableView, indexPath: indexPath)
           default: break
        }
    }
    
    internal func setCell(withSection section: String,withCell cell:UITableViewCell,withIndexPath indexPath: IndexPath) -> UITableViewCell {
        switch section {
           case "Export":
            setExportCell(withCell: cell, withIndexPath: indexPath)
                return cell
           case "List":
            setListCell(withCell: cell, withIndexPath: indexPath)
               return cell
           case "ModeCheck":
            setModeCheckCell(withCell: cell, withIndexPath: indexPath)
               return cell
           case "AreaSector":
            setAreaCell(withCell: cell, withIndexPath: indexPath)
               return cell
           default:
             return cell
        }
    }
    
}
