//
//  AppDelegate.swift
//  CFScanner
//
//  Created by Alessandro de Peppo on 20/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//


import UIKit
import SwiftUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var loadLogsTimer : DispatchSourceTimer?
    var loadStewardTimer : DispatchSourceTimer?
    var syncLogsTimer : DispatchSourceTimer?
    var arrSectors : [String] = []
    var arrAreasAndSectors = [String : [String]]()
    var arrAreasAndSectorsFinal = [String : [String]]()
    let viewHosting = UIHostingController(rootView: LoginView())
    var event : Int?
    
    var threadLoadStewards : Thread?
    var threadAddStewards : Thread?
    var threadLoadLogs : Thread?
    var threadAddLogs : Thread?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if UserDefaults.standard.value(forKey: firstOpenApp) as? String != nil{
            print("save preferences firstOpenApp")
        }else{
            self.setFirstOpenAppAndSetModeCamera()
        }
        if UserDefaults.standard.value(forKey: usernameApp) as? String != nil{
            print("user saved")
            //check if areas and events have been previously saved
            if UserDefaults.standard.value(forKey: areasAndSectors) != nil{
                let arr = UserDefaults.standard.value(forKey: areasAndSectors) as! [String : [String]]
                self.arrAreasAndSectors = arr
                self.arrAreasAndSectorsFinal = arr
            }
            //set Credentials and load Events
            self.setCredentialsAndLoadEvents()
        }else{
            //load view Login
            self.loadLoginView()
        }
        
        return true
    }

    @objc func runLoadStewards() {
        let backgroundQueue = DispatchQueue(label: "runLoadStewards.queue", qos: .background)
        backgroundQueue.async {
            print("Run on background thread")
            DispatchQueue.delay(.milliseconds(300)) {
                APIManager.shared.loadStewards(forEvent: self.event!, onSuccess: { checkLoadSteward  in
                    DispatchQueue.main.async {
                        print("loadStewards \(checkLoadSteward)")
                    }
                },onFailure: { strError  in
                    DispatchQueue.main.async {
                        RetryManager.sharedInstance.startRetry(retryCount: 3) { (result) in
                            if(result){
                                DispatchQueue.delay(.milliseconds(1500)) {
                                    self.runAddStewards()
                                }
                            }else{
                                DispatchQueue.main.async {
                                    print(strError)
                                    print("loadStewards Error")
                                }
                            }
                        }
                    }
                })
            }
        }
    }
    
    @objc func runAddStewards() {
        let backgroundQueue = DispatchQueue(label: "runAddStewards.queue", qos: .background)
        backgroundQueue.async {
            DispatchQueue.delay(.milliseconds(300)) {
                APIManager.shared.addStewards(forEvent: self.event!, onSuccess: { checkAddSteward  in
                    DispatchQueue.main.async {
                        print("addStewards \(checkAddSteward)")
                    }
                },onFailure: { strError  in
                    DispatchQueue.main.async {
                        RetryManager.sharedInstance.startRetry(retryCount: 3) { (result) in
                            if(result){
                                DispatchQueue.delay(.milliseconds(1500)) {
                                    self.runAddStewards()
                                }
                            }else{
                                DispatchQueue.main.async {
                                    print(strError)
                                    print("addStewards Error")
                                }
                            }
                        }
                    }
                })
            }
        }
    }
    
    @objc func runLoadLogs() {
        let backgroundQueue = DispatchQueue(label: "runLoadLogs.queue", qos: .background)
        backgroundQueue.async {
            DispatchQueue.delay(.milliseconds(300)) {
                APIManager.shared.loadLogs(forEvent: self.event!, onSuccess: { checkLoadLogs  in
                    DispatchQueue.main.async {
                        print("loadLogs \(checkLoadLogs)")
                    }
                },onFailure: { strError  in
                    DispatchQueue.main.async {
                        RetryManager.sharedInstance.startRetry(retryCount: 3) { (result) in
                            if(result){
                                DispatchQueue.delay(.milliseconds(1500)) {
                                    self.runLoadLogs()
                                }
                            }else{
                                DispatchQueue.main.async {
                                    print(strError)
                                    print("loadLogs Error")
                                }
                            }
                        }
                    }
                })
            }
        }
        
    }
    
    @objc func runAddLogs() {
        let backgroundQueue = DispatchQueue(label: "runAddLogs.queue", qos: .background)
        backgroundQueue.async {
            DispatchQueue.delay(.milliseconds(300)) {
                APIManager.shared.addLogs(forEvent: self.event!, onSuccess: { checkAddLogs  in
                    DispatchQueue.main.async {
                        print("addLogs \(checkAddLogs)")
                    }
                },onFailure: { strError  in
                    DispatchQueue.main.async {
                        RetryManager.sharedInstance.startRetry(retryCount: 3) { (result) in
                            if(result){
                                DispatchQueue.delay(.milliseconds(1500)) {
                                    self.runAddLogs()
                                }
                            }else{
                                DispatchQueue.main.async {
                                    print(strError)
                                    print("addLogs Error")
                                }
                            }
                        }
                    }
                })
            }
        }
        
    }
        
    func startBackgroundProcs(forEvent event : Int) {
        if let syncLogsTimer = syncLogsTimer, (!syncLogsTimer.isCancelled) {
            return
        }
        print("Starting Background Processes form event \(event)")
        self.event = event
        
        if syncLogsTimer == nil {
            syncLogsTimer = DispatchSource.makeTimerSource(queue: .main)
            syncLogsTimer?.schedule(deadline: DispatchTime.now()+6.0, repeating: 20.0)
            syncLogsTimer?.setEventHandler {
                DispatchQueue.main.async {
                    self.runAddLogs()
                }
            }
            syncLogsTimer?.resume()
        }
                    
        if loadStewardTimer == nil {
            loadStewardTimer = DispatchSource.makeTimerSource(queue: .main)
            loadStewardTimer?.schedule(deadline: .now(), repeating: 20.0)
            loadStewardTimer?.setEventHandler {
                DispatchQueue.main.async {
                    self.runLoadStewards()
                    self.runAddStewards()
                }
            }
            loadStewardTimer?.resume()
        }
        
        if loadLogsTimer == nil {
            loadLogsTimer = DispatchSource.makeTimerSource(queue: .main)
            loadLogsTimer?.schedule(deadline: .now()+12.0, repeating: 20.0)
            loadLogsTimer?.setEventHandler {
                DispatchQueue.main.async {
                    self.runLoadLogs()
                }
            }
            loadLogsTimer?.resume()
        }
            
    }
    
    func stopBackgrounProcs() {
        print("Stopping Background Processes")
        syncLogsTimer?.cancel()
        loadStewardTimer?.cancel()
        loadLogsTimer?.cancel()

        syncLogsTimer = nil
        loadStewardTimer = nil
        loadLogsTimer = nil
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.set(false, forKey: "Light")
        Torch().turnOffTorch()

    }
    
    //Function save firstOpenApp and set Mode Camera
    func setFirstOpenAppAndSetModeCamera(){
        UserDefaults.standard.set("0", forKey: firstOpenApp)
        SettingsManager.sharedInstance.saveModeCheck = "ModeCheck0"
    }
    
    //Function reset UserDefaults
    func resetUserDefaults() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: usernameApp)
        userDefaults.removeObject(forKey: passwordApp)
        userDefaults.removeObject(forKey: areasAndSectors)
        UserDefaults.standard.set(0, forKey: "event")
        userDefaults.synchronize()
    }
    
    //load view Login
    func loadLoginView(){
        viewHosting.modalPresentationStyle = .fullScreen
        DispatchQueue.delay(.milliseconds(300)) { [self] in
            self.window?.rootViewController?.present(viewHosting, animated: true, completion: nil)
        }
    }
    
    //set Credentials and Download Events
    func setCredentialsAndLoadEvents(){
        let username = UserDefaults.standard.value(forKey: usernameApp) as! String
        let password = UserDefaults.standard.value(forKey: passwordApp) as! String
        let cred = Credentials(username: username, password: password)
        let loginString = "\(cred.username):\(cred.password)"
        guard let loginData = loginString.data(using: String.Encoding.utf8) else {
            APIManager.shared.base64Credentials = nil
            return
        }
        APIManager.shared.base64Credentials = loginData.base64EncodedString()
        DispatchQueue.delay(.milliseconds(100)) {
            APIManager.shared.loadEvents(onSuccess: { checkEvent  in
                DispatchQueue.main.async {
                    print("loadEvents \(checkEvent)")
                }
            },onFailure: { strError  in
                DispatchQueue.main.async {
                    print(strError)
                    print("loadEvents Error")
                }
            })
        }
    }
    
    func performOn(_ queueType: QueueType, closure: @escaping () -> Void) {
        queueType.queue.async(execute: closure)
    }


}

