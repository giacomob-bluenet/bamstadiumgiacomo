//
//  LazioCsvCreateExt.swift
//  Steward Check-in lazio
//
//  Created by Alessandro de Peppo on 04/02/2020.
//  Copyright © 2020 Bluenet. All rights reserved.
//

import Foundation

extension CsvCreate {
    
    class func getHeader() -> String {
            let header = "\(NSLocalizedString("company", comment: "" ))"
                + ";" + "\(NSLocalizedString("sector", comment: "" ))"
                + ";" + "\(NSLocalizedString("surnameField", comment: "" ))"
                + ";" + "\(NSLocalizedString("nameField", comment: "" ))"
                + ";" + "\(NSLocalizedString("dobField", comment: "" ))"
                + ";" + "\(NSLocalizedString("cityField", comment: "" ))"
                + ";" + "\(NSLocalizedString("cfField", comment: "" ))"
                + ";" + "\(NSLocalizedString("CheckedTime", comment: "" ))"
                + ";" + "\(NSLocalizedString("CheckOutTime", comment: "" ))\n"
        return header
    }
    
    class func getFormattedRow(for item : Steward) -> String {
        let dob = DateFormatter.df.string(for: item.dob)
        let checkedTime = DateFormatter.dtf.string(for: item.checkTime)
        let checkOutTime = DateFormatter.dtf.string(for: item.checkOutTime)
        let sector = (item.sector != nil ? item.sector! : "")
        let row = "\(item.area ?? "")"
            + ";" + "\(sector)"
            + ";" + "\(item.familyName ?? "")"
            + ";" + "\(item.givenName ?? "")"
            + ";" + "\(dob ?? "")"
            + ";" + "\(item.city ?? "")"
            + ";" + "\(item.cf ?? "")"
            + ";" + "\(checkedTime ?? "")"
            + ";" + "\(checkOutTime ?? "")\n"
        return row;
    }
}
