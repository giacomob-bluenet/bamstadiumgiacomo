//
//  ScannerView.swift
//  Steward Check-in
//
//  Created by Youssef on 30/01/2020.
//  Copyright © 2020 KM, Abhilash. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension ScannerView {
    func setCode(metadataOutput: AVCaptureMetadataOutput){
        metadataOutput.metadataObjectTypes = [.code39,.code128]
    }
    
    func findCode(withCode code:String?,withType type: AVMetadataObject.ObjectType) -> String? {
        var finalCode : String?
        
        if(type == .code39){
            return code
        } else {
            if((code?.hasPrefix("1"))!) {
                if let number = Int(String((code?.dropFirst())!)) {
                    finalCode = String(number)
                }
            } else {
                if let number = Int(code!) {
                    finalCode = String(number)
                }
            }
        }
        
        return finalCode
    }
}

