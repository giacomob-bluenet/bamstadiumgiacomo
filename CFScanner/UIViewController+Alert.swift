//
//  UIViewController+Alert.swift
//  CFScanner
//
//  Created by Alessandro de Peppo on 20/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

// MARK: DispatchQueue

extension DispatchQueue {
    static func delay(_ delay: DispatchTimeInterval, closure: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: closure)
    }
}

// MARK: UIApplication

extension UIApplication: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true // set to `false` if you don't want to detect tap during other gestures
    }
}

extension UIApplication {
    
    func addTapGestureRecognizer() {
        guard let window = windows.first else { return }
        let tapGesture = UITapGestureRecognizer(target: window, action: #selector(UIView.endEditing))
        tapGesture.requiresExclusiveTouchType = false
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        window.addGestureRecognizer(tapGesture)
    }
    
    var currentWindow: UIWindow? {
        connectedScenes
        .filter({$0.activationState == .foregroundActive})
        .map({$0 as? UIWindowScene})
        .compactMap({$0})
        .first?.windows
        .filter({$0.isKeyWindow}).first
    }
    var topViewController: UIViewController?{
        let keyWindow = Array(UIApplication.shared.connectedScenes)
                .compactMap { $0 as? UIWindowScene }
                .flatMap { $0.windows }
                .first(where: { $0.isKeyWindow })
        if keyWindow == nil{
            return keyWindow?.rootViewController
        }
        var pointedViewController = keyWindow?.rootViewController
        while  pointedViewController?.presentedViewController != nil {
            switch pointedViewController?.presentedViewController {
            case let navagationController as UINavigationController:
                pointedViewController = navagationController.viewControllers.last
            case let tabBarController as UITabBarController:
                pointedViewController = tabBarController.selectedViewController
            default:
                pointedViewController = pointedViewController?.presentedViewController
            }
        }
        return pointedViewController
    }
}

// MARK: UIView

extension UIView {
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.cornerRadius = 5.0
    }
    
}

// MARK: UIViewController
var viewSpinner : UIView?

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                             action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showToast(message : String, seconds: Double = 2.0) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        self.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
    
    func alertRequiredPassword() {
        let alertController = UIAlertController(title: NSLocalizedString("name_app", comment: ""),
                                           message: NSLocalizedString("insert_password", comment: ""),
                                           preferredStyle: .alert)
        alertController.addTextField(
            configurationHandler: {(textField: UITextField!) -> Void in
                textField.placeholder = NSLocalizedString("password", comment: "")
                textField.clearButtonMode = UITextField.ViewMode.whileEditing;
                textField.isSecureTextEntry = true
                textField.becomeFirstResponder()
        })
        let textField = alertController.textFields![0] as UITextField
        let passwordAction = UIAlertAction(title: NSLocalizedString("send", comment: ""), style: .default,
                                         handler: {(paramAction :UIAlertAction!) in
            
            let password = textField.text! as String
            if(password == passwordSettings){
                self.performSegue(withIdentifier: "settings", sender: nil)
            }else{
                self.alertErrorPassword()
            }
                                            
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("annul", comment: ""), style: .destructive, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        alertController.addAction(cancelAction)
        alertController.addAction(passwordAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    func alertErrorPassword(){
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_password", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertErrorNoSectors(){
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_no_sectors", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertErrorNoAreas(){
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_no_areas", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertErrorGeneral(){
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_general", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertNoAreasAndSectors(){
        let alert = UIAlertController(title: NSLocalizedString("attention", comment: ""), message: NSLocalizedString("error_no_areas_sectors", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertErrorEvents(){
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_events", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertErrorLogs(){
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_logs_zero", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertConfirmSettings(event: Int) {
        let alertController = UIAlertController(title: NSLocalizedString("name_app", comment: ""),
                                           message: NSLocalizedString("message_confirm_settings", comment: ""),
                                           preferredStyle: .alert)
        let okayAction = UIAlertAction(title: NSLocalizedString("send", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.confirmSettings(event: event)
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("annul", comment: ""), style: .destructive, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okayAction)
        self.present(alertController, animated: true, completion:nil)

    }
    
    func confirmSettings(event: Int){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        DataManager.shared.persistentContainer.performBackgroundTask{ [self](context) in
            DataManager.shared.removeAllSteward(withContext: context)
            DataManager.shared.removeAllLog(withContext: context)
            let key = "lastCode." + "\(event)"
            UserDefaults.standard.set("0", forKey: key)
            UserDefaults.standard.synchronize()
            appDelegate.arrAreasAndSectorsFinal = appDelegate.arrAreasAndSectors
            print(appDelegate.arrAreasAndSectorsFinal)
            UserDefaults.standard.set(appDelegate.arrAreasAndSectorsFinal, forKey: areasAndSectors)
            UserDefaults.standard.synchronize()
            SettingsViewController.instance.setChangeEvent()
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func showSpinner(onView : UIView?) {
        DispatchQueue.main.async {
            let spinnerView = UIView.init(frame: onView!.bounds)
            spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
            let ai = UIActivityIndicatorView(frame: onView!.bounds)
            ai.tintColor = .red
            ai.startAnimating()
            spinnerView.addSubview(ai)
            if(viewSpinner == nil){
                onView!.addSubview(spinnerView)
                viewSpinner = spinnerView
            }
        }
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            viewSpinner?.removeFromSuperview()
            viewSpinner = nil
        }
    }
    
    func closeVC() {
        guard (navigationController?.popViewController(animated:true)) != nil
            else {
                dismiss(animated: true, completion: nil)
                return
        }
    }
    
    @objc func closeView(){
        self.closeVC()
    }
    
    func alertErrorMessage(_ stringError: String){
        var messageError : String!
        if(stringError != ""){
            messageError = stringError
        }else{
            messageError = NSLocalizedString("error_general", comment: "")
        }
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""), message: messageError, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

// MARK: String

extension String {
    func trimString() -> String {
          return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}


// MARK: SWIFTUI

extension View {
    
    @ViewBuilder func isHidden(_ hidden: Bool, remove: Bool = false) -> some View {
        if hidden {
            if !remove {
                self.hidden()
            }
        } else {
            self
        }
    }
    
}
