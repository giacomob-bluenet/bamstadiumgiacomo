//
//  StewardListViewController.swift
//  Steward Check-in
//
//  Created by Youssef on 02/10/2019.
//  Copyright © 2019 KM, Abhilash. All rights reserved.
//

import UIKit

class StewardListViewController: UIViewController {
   
    var event : Int = 0
    var stewards: [Steward]?
    var filterStewards = [Steward]()
    private var modeType : ModeType = .checkIn
    let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var stewardTableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Cerca steward.."
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        // Setup the Table View
        stewardTableView.delegate = self
        stewardTableView.dataSource = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(openAddSteward))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Set mode type ( check-in or check-out )
        modeType = ModeType(rawValue: UserDefaults.standard.integer(forKey: "Mode")) ?? .checkIn
        stewards = DataManager.shared.findSteward(forEvent: event, withCheckList: List(rawValue: segmentedControl.selectedSegmentIndex) ?? List.unChecked,ModeType: modeType)
        stewardTableView.reloadData()
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = true
        }
    }
    
    @objc func openAddSteward() {
        self.performSegue(withIdentifier: "addSteward", sender: nil)
    }
    
    @IBAction func indexChanged(_ sender: Any) {
        stewards = DataManager.shared.findSteward(forEvent: event, withCheckList: List(rawValue: segmentedControl.selectedSegmentIndex) ?? List.unChecked,ModeType: modeType)
        stewardTableView.reloadData()

    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "addSteward") {

            if let stewardCreateViewController = segue.destination as? StewardCreateViewController, sender == nil {
                stewardCreateViewController.event = event
            }else if let stewardCreateViewController = segue.destination as? StewardCreateViewController , sender != nil {
                stewardCreateViewController.event = event
                stewardCreateViewController.steward = sender as? Steward
                stewardCreateViewController.modeType = modeType
            }
        }
    }
    

}
// MARK: - UITableViewDelegate/DataSource

extension StewardListViewController: UITableViewDelegate,UITableViewDataSource {
    
    func isFiltering() -> Bool {
      return searchController.isActive && !searchBarIsEmpty()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
          return filterStewards.count
        }
          
        return stewards?.count ?? 0
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
       let steward: Steward
       if isFiltering() {
        steward = filterStewards[indexPath.row]
       } else {
        steward = stewards![indexPath.row]
       }
        
        cell.textLabel?.text = (steward.familyName ?? "") + " " + (steward.givenName ?? "")
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        cell.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.detailTextLabel?.text = setDetailLabel(steward: steward)
        cell.detailTextLabel?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        
       return cell
    }
    
    
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        if(segmentedControl.selectedSegmentIndex == 0){
            let steward: Steward
            if isFiltering() {
             steward = filterStewards[indexPath.row]
            } else {
             steward = stewards![indexPath.row]
            }
            
            let checkInAction = UIContextualAction(style: .normal, title:  self.modeType == .checkIn ? "Check-in" : "Check-out", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                
                    let checkTime = Date.init()
                    if (self.modeType == .checkIn) {
                        steward.checkTime = checkTime
                        steward.checkIn = CheckIn.local.rawValue
                    } else {
                        steward.checkOutTime = checkTime
                        steward.checkOut = CheckOut.local.rawValue
                    }
              
                    DataManager.shared.update(steward)
                DataManager.shared.addLog(forEvent: self.event, cf: steward.cf!, withTimestamp: checkTime, andStatus: self.modeType == .checkIn ? "OK" : "OUT")

                    //need to remove the element from the array before delete the row
                    if self.isFiltering() {
                      self.filterStewards.remove(at: indexPath.row)
                    } else {
                      self.stewards?.remove(at: indexPath.row)
                    }
                
                    self.stewardTableView.deleteRows(at: [indexPath], with: .automatic)
                    success(true)
                
                })
                checkInAction.backgroundColor = .blue
            
            return UISwipeActionsConfiguration(actions: [checkInAction])

        }else{
            let swipeAction = UISwipeActionsConfiguration(actions: [])

            return swipeAction
        }
        
    
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        if(segmentedControl.selectedSegmentIndex == 1 && modeType == .checkIn){
            let steward: Steward
            if isFiltering() {
             steward = filterStewards[indexPath.row]
            } else {
             steward = stewards![indexPath.row]
            }
            
            let checkOutAction = UIContextualAction(style: .normal, title:  "Reset", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                    
                    //Reset all value
                    steward.checkTime = nil
                    steward.checkOutTime = nil
                    steward.checkIn = CheckIn.noCheck.rawValue
                    steward.checkOut = CheckOut.noCheck.rawValue
                
                    DataManager.shared.update(steward)
                    DataManager.shared.addLog(forEvent: self.event, cf: steward.cf!, withTimestamp: Date.init(), andStatus: "RESET")

                    //need to remove the element from the array before delete the row
                    if self.isFiltering() {
                      self.filterStewards.remove(at: indexPath.row)
                    } else {
                      self.stewards?.remove(at: indexPath.row)
                    }
                
                    self.stewardTableView.deleteRows(at: [indexPath], with: .automatic)
                    success(true)
                })
                checkOutAction.backgroundColor = .red
            
            return UISwipeActionsConfiguration(actions: [checkOutAction])

        }else{
            let swipeAction = UISwipeActionsConfiguration(actions: [])

            return swipeAction
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let steward: Steward
        if isFiltering() {
         steward = filterStewards[indexPath.row]
        } else {
         steward = stewards![indexPath.row]
        }
        
        let visibleBtnCheckIn = self.segmentedControl.selectedSegmentIndex
        print(visibleBtnCheckIn)
        print("self.segmentedControl.selectedSegmentIndex")
        print("aaaaaah")

        let storyboard = UIStoryboard(name: "InfoTicket", bundle: nil)
        let vc : InfoTicketViewController = (storyboard.instantiateViewController(withIdentifier: "infoticketVC") as? InfoTicketViewController)!
        vc.steward = steward
        vc.event = event
        vc.modeType = modeType
        if visibleBtnCheckIn == 0 {
            vc.visibleBtnCheckIn = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
       
        //self.performSegue(withIdentifier: "addSteward", sender: steward)
    }
       
}

// MARK: - UISearchResultsUpdating

extension StewardListViewController: UISearchResultsUpdating {
    
   // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
      filterContentForSearchText(searchController.searchBar.text!)
    }
    
    // MARK: - Private instance methods
      
    func searchBarIsEmpty() -> Bool {
      // Returns true if the text is empty or nil
      return searchController.searchBar.text?.isEmpty ?? true
    }
      
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        //check array dimension
        if stewards != nil {
            filterStewards = stewards!.filter({( steward : Steward) -> Bool in
                filterResults(steward: steward,searchText: searchText.lowercased())
            })
        }
    
      stewardTableView.reloadData()
    }
}

