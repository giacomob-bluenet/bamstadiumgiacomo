//
//  DataController.swift
//  CFScanner
//
//  Created by Alessandro de Peppo on 19/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//

import UIKit
import CoreData
class DataManager: NSObject {
    
    static let shared = DataManager()
    static let fetchLimit = 100

    var mainContext : NSManagedObjectContext?

    private override init() {
        
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy public var viewContext: NSManagedObjectContext = {
        var viewContext = self.persistentContainer.newBackgroundContext()
        viewContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        viewContext.automaticallyMergesChangesFromParent = true
        return viewContext
    }()
    
    func saveAndWaitDerivedContext(_ context: NSManagedObjectContext) throws {
        let observer = NotificationCenter.default.addObserver(forName: .NSManagedObjectContextDidSave, object: context, queue: .main) { (notification) in
            self.mainContext!.mergeChanges(fromContextDidSave: notification)
        }
        context.performAndWait {
            try? context.save()
        }
        NotificationCenter.default.removeObserver(observer)
    }
}
