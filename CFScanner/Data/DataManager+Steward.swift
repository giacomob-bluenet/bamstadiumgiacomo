//
//  DataManager+Steward.swift
//  bluecode-reader
//
//  Created by Alessandro de Peppo on 20/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//

import Foundation
import CoreData

extension DataManager {
    
    /**
     Get ticket from database using viewContext
     - Parameter card: The code to be checked
     - Parameter event: The eventId to be checked
     - Returns: steward entry
     */
    func getSteward(_ code: String, forEvent event: Int) -> Steward? {
        let conetxt = persistentContainer.viewContext
        return getStewardWithContext(conetxt, code, forEvent: event)
    }
    
    /**
     Get steward from database
     - Parameter context: NSManagedObhject context
     - Parameter code: The code to be checked
     - Parameter event: The eventId to be checked
     - Returns: steward entry
     */
    func getStewardWithContext(_ context:  NSManagedObjectContext, _ code: String, forEvent event: Int) -> Steward? {
        return getGenericItemWithContext(context, code, forEvent: event)
    }
    
    /**
     Get steward from database
     - Parameter context: NSManagedObhject context
     - Parameter code: The code to be checked
     - Parameter type: The CardType to be checked
     - Parameter event: The eventId to be checked
     - Returns: ticket entry
     */
    func getGenericItemWithContext(_ context:  NSManagedObjectContext, _ code: String, forEvent event: Int) -> Steward? {
        let request : NSFetchRequest<Steward> = Steward.fetchRequest()
        let codeKeyPredicate = NSPredicate(format: "cf = [c]%@ OR extra = %@", code, code)
        let eventKeyPredicate = NSPredicate(format: "event = %d", event)
        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [codeKeyPredicate, eventKeyPredicate])
        request.predicate = andPredicate
        
        do {
            let items = try context.fetch(request)
            if items.count > 0  {
                return items[0]
            }else{
                return nil
            }
        } catch {
          print("error is \(error.localizedDescription)")
            return nil
        }

    }
    
    /*func getGenericItemWithContext(_ context:  NSManagedObjectContext, _ code: String, forEvent event: Int) -> Steward? {
        let request : NSFetchRequest<Steward> = Steward.fetchRequest()
        let codeKeyPredicate = NSPredicate(format: "cf = [c]%@ OR extra = %@", code, code)
        let eventKeyPredicate = NSPredicate(format: "event = %d", event)
        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [codeKeyPredicate, eventKeyPredicate])
        request.predicate = andPredicate
        
        do {
            let items = try context.fetch(request)
            if items.count > 0  {
                return items[0]
            } else {
                return nil;
            }
        } catch {
            print("Failed to fetch items: \(error)")
        }
        return nil
    }*/
    
    func countUncheck(withCheck check: [Int16], forEvent event : Int,ModeType modeType: ModeType) -> Int? {
        let checkedKeyPredicate = modeType == .checkIn ? NSPredicate(format: "checkIn IN %@ ", check) : NSPredicate(format: "checkIn IN %@ AND checkOut IN %@", check, [CheckOut.noCheck.rawValue])
        return count(withCheck: check, forEvent: event, withPredicate: checkedKeyPredicate)
    }
    
    func countAll(forEvent event : Int) -> Int? {
        let context = persistentContainer.viewContext
        let request : NSFetchRequest<Steward> = Steward.fetchRequest()
        let eventKeyPredicate = NSPredicate(format: "event = %d AND valid = 1", event)
        request.predicate = eventKeyPredicate
        do {
            let count : Int = try context.count(for: request)
            return count
        } catch {
            print("Failed to count cards: \(error)")
        }
        return nil
    }
    
    func countChecked(withCheck check: [Int16], forEvent event : Int, ModeType modeType: ModeType) -> Int? {
        let checkedKeyPredicate = modeType == .checkIn ? NSPredicate(format: "checkIn IN %@", check) : NSPredicate(format: "checkOut IN %@", check)
        return count(withCheck: check, forEvent: event, withPredicate: checkedKeyPredicate)
    }
    
    
    func count(withCheck check: [Int16], forEvent event : Int,withPredicate predicate: NSPredicate) -> Int? {
        let context = persistentContainer.viewContext
        let request : NSFetchRequest<Steward> = Steward.fetchRequest()
        let eventKeyPredicate = NSPredicate(format: "event = %d AND valid = 1", event)
        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate,  eventKeyPredicate])
        
        request.predicate = andPredicate
        do {
            let count : Int = try context.count(for: request)
            return count
        } catch {
            print("Failed to count cards: \(error)")
        }
        return nil
    }
    
    func update(_ ticket: Steward) {
        let context = persistentContainer.viewContext
        do {
            try context.save()
        } catch {
            print("Failure to save context: \(error)")
            context.rollback()
        }
    }
    
    
    /**
     Find a list steward in viewContext
     - Parameter check: true for checked; false for unchecked
     - Parameter event: The eventId to be checked
     - Returns: a stward entity or nil
     */
    func findSteward(forEvent event: Int, withCheckList check: List, ModeType modeType: ModeType) -> [Steward]? {
        let context = persistentContainer.viewContext
        switch check {
        case List.unChecked:
            let checkIn = modeType == .checkIn ? [CheckIn.noCheck.rawValue] : [CheckIn.local.rawValue,CheckIn.external.rawValue]
            let checkOut =  [CheckOut.noCheck.rawValue]
            let familyNameSort = NSSortDescriptor(key:"familyName", ascending:true)
            let givenNameSort = NSSortDescriptor(key:"givenName", ascending:true)
            return findSteward(context, forEvent: event, withCheckIn: checkIn, withCheckOut: checkOut, withSortDescriptor: [familyNameSort, givenNameSort])
        case List.Checked:
            let checkIn = [CheckIn.local.rawValue,CheckIn.external.rawValue]
            let checkOut =  modeType == .checkIn ? [CheckOut.noCheck.rawValue,CheckOut.local.rawValue,CheckOut.external.rawValue] : [CheckOut.local.rawValue,CheckOut.external.rawValue]
            let checktimeSort = modeType == .checkIn ? NSSortDescriptor(key:"checkTime", ascending:false) : NSSortDescriptor(key:"checkOutTime", ascending:false)
            return findSteward(context, forEvent: event, withCheckIn: checkIn, withCheckOut: checkOut, withSortDescriptor: [checktimeSort])
        default:
            return nil
        }
    }
    
    /**
        Find a list steward in viewContext
        - Parameter check: true for checked; false for unchecked
        - Parameter event: Multiple eventId to be checked
        - Returns: a stward entity or nil
        */
       func findSteward(forEvent event: Int) -> [Steward]? {
           let context = persistentContainer.viewContext
           let checkIn = [CheckIn.local.rawValue,CheckIn.external.rawValue]
        let checkOut = [CheckOut.noCheck.rawValue,CheckOut.local.rawValue,CheckOut.external.rawValue]
           let cfSort = NSSortDescriptor(key:"cf", ascending:false)
           return findSteward(context, forEvent: event, withCheckIn: checkIn, withCheckOut: checkOut, withSortDescriptor: [cfSort])
       }
    
    /**
     Find a list of unchecked steward in the given context
     - Parameter context: NSManagedObhject context
     - Parameter check: true for checked; false for unchecked
     - Parameter event: The eventId to be checked
     - Returns: a stward entity or nil
     */
    func findSteward(_ context:  NSManagedObjectContext, forEvent event: Int, withCheckIn checkIn: [Int16],withCheckOut checkOut: [Int16],withSortDescriptor sortDescriptor: [NSSortDescriptor]) -> [Steward]? {
        let request : NSFetchRequest<Steward> = Steward.fetchRequest()
        let codeKeyPredicate = NSPredicate(format: "event = %d and valid = 1", event)
        let eventKeyPredicate = NSPredicate(format: "checkIn IN %@ AND checkOut IN %@", checkIn, checkOut)
        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [codeKeyPredicate, eventKeyPredicate])
        request.predicate = andPredicate
        request.sortDescriptors = sortDescriptor
        
        do {
            let stewards = try context.fetch(request)
            if stewards.count > 0  {
                return stewards
            } else {
                return nil;
            }
        } catch {
            print("Failed to fetch stewards: \(error)")
        }
        return nil
    }
    
    
    /**
     Fetch not synced Steward
     - Parameter event: The eventId to sync. If nil, any events wil be synced
     - Returns: List of Stewards, if any
     */
    func fetchNewStewards(forEvent event: Int?, withContext ctx: NSManagedObjectContext) -> [Steward]? {
        let request : NSFetchRequest<Steward> = Steward.fetchRequest()
        request.fetchLimit = DataManager.fetchLimit
        let syncKeyPredicate = NSPredicate(format: "sync = 0")
        if let event = event {
            let eventKeyPredicate = NSPredicate(format: "event = %d", event)
            let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [syncKeyPredicate, eventKeyPredicate])
            request.predicate = andPredicate
        } else {
            request.predicate = syncKeyPredicate
        }
        
        do {
            let stewards = try ctx.fetch(request)
            return stewards
        } catch {
            print("Failed to fetch stewards: \(error)")
        }
        return nil
    }
    
    /**
     Remove all steward in db
     */
    func removeAllSteward(withContext ctx: NSManagedObjectContext){
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Steward")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try ctx.execute(deleteRequest)
            try ctx.save()
        } catch {
            print ("error")
        }
    }


}

