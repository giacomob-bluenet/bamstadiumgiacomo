//
//  SettingsViewController.swift
//  Steward Check-in
//
//  Created by Alessandro de Peppo on 13/09/2019.
//  Copyright © 2019 KM, Abhilash. All rights reserved.
//

import UIKit
import CoreData


class SettingsViewController: UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var arrAreas : [String] = []
    var arrAreasTemp : [String] = []

    var currentEvent : Int?
    var changeEvent : Bool = false
    var section: [String] = []
    var events : [Event]?
    internal var lastRowForSection = [0,0]
    
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var btnLogout: UIButton!
    private(set) static var instance: SettingsViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SettingsViewController.instance = self
        section = setSection()
        
        //check if areas and events have been previously saved
        if UserDefaults.standard.value(forKey: areasAndSectors) != nil{
            let arr = UserDefaults.standard.value(forKey: areasAndSectors) as! [String : [String]]
            self.appDelegate.arrAreasAndSectors = arr
        }
        
        //refresh for events
        self.refreshControl = UIRefreshControl()
        if let refreshControl = self.refreshControl {
            refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("refresh_events", comment: ""))
            refreshControl.addTarget(self, action: #selector(self.refreshEvents(_:)), for: .valueChanged)
            tableView.addSubview(refreshControl)
        }
        
        self.btnLogout.backgroundColor = .white
        self.btnLogout.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        self.btnLogout.setTitle(NSLocalizedString("logout", comment: ""), for: .normal)
        self.btnLogout.dropShadow()
        self.viewLogout.backgroundColor = .white

        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: self.viewLogout.frame.height, right: 0.0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: self.viewLogout.frame.height, right: 0.0)
        self.viewLogout.frame.origin.x = self.view.frame.origin.x
        self.viewLogout.frame.origin.y = self.tableView.bounds.origin.y + self.tableView.frame.height - 80
        self.viewLogout.autoresizingMask = UIView.AutoresizingMask.flexibleTopMargin
        self.view.addSubview(self.viewLogout)
        
        self.tableView.layoutIfNeeded()
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if UserDefaults.standard.value(forKey: usernameApp) as? String != nil{
            saveSettings()
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let frame = CGRect(x: self.viewLogout.frame.origin.x, y: self.tableView.bounds.origin.y + self.tableView.frame.height - 80, width: self.tableView.frame.size.width, height: self.viewLogout.frame.size.height)
        viewLogout.frame = frame
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return section.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString(self.section[section], comment: "")
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return numberOfRowsInSection(withSection: self.section[section])
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        return setCell(withSection: section[indexPath.section], withCell: cell, withIndexPath: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        handleRowSelection(withSection: section[indexPath.section],withTableView: tableView,withIndexPath: indexPath)
    }
    
    // Set Cell
    internal func setExportCell(withCell cell:UITableViewCell,withIndexPath indexPath: IndexPath) {
        cell.textLabel?.text = NSLocalizedString("Export\(indexPath.row)", comment: "")
        cell.textLabel?.textColor = .systemBlue
        cell.textLabel?.highlightedTextColor = .systemBlue
    }
    
    //Cell for camera or infrared mode
    internal func setModeCheckCell(withCell cell:UITableViewCell,withIndexPath indexPath: IndexPath) {
        cell.textLabel?.text = NSLocalizedString("ModeCheck\(indexPath.row)", comment: "")
        cell.textLabel?.textColor = .black
        let check = SettingsManager.sharedInstance.saveModeCheck
        if(check == "ModeCheck\(indexPath.row)"){
            cell.accessoryType = .checkmark
            cell.tintColor = .systemBlue
        }else{
            cell.accessoryType = .none
        }
    }
    
    //Cell for Area
    internal func setAreaCell(withCell cell:UITableViewCell,withIndexPath indexPath: IndexPath) {
        let area = self.arrAreas[indexPath.row]
        cell.textLabel?.text = area
        cell.textLabel?.textColor = .black

        if(appDelegate.arrAreasAndSectors.count > 0){
            let keyExists = appDelegate.arrAreasAndSectors[area] != nil
            if keyExists {
                cell.accessoryType = .checkmark
                cell.tintColor = .systemBlue
            }else {
                cell.accessoryType = .none
            }
        }else{
            cell.accessoryType = .none
        }
    }
    
    internal func setListCell(withCell cell:UITableViewCell,withIndexPath indexPath: IndexPath) {
        if indexPath.row < events?.count ?? 0 {
            cell.textLabel?.text = events?[indexPath.row].name
            cell.textLabel?.textColor = UIColor.black
            if let id = events?[indexPath.row].id, Int(id) == (currentEvent) {
                lastRowForSection[0] = indexPath.row
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                cell.accessoryType = .checkmark
                cell.tintColor = .systemBlue
            }else{
                cell.accessoryType = .none
            }
        }else{
            cell.textLabel?.text = ""
        }
    }
    
    // Handle selection row
    internal func handleExport(tableView: UITableView,indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        var match = ""
        if let e = self.currentEvent, let rowId = events?.lastIndex(where:{ $0.id == e })  {
            match = (events?[rowId].name)!
        }
        if (indexPath.row == 0){
            DispatchQueue.main.async {
                let dati = CsvCreate.createExportString(event: Int(self.currentEvent ?? 0))
                CsvCreate.saveAndExport(exportString: dati, viewController: self,eventName: match)
            }
        }else{
            let dati = CsvCreate.createExportString(event: Int(self.currentEvent ?? 0))
            CsvCreate.saveAndExportinFileShare(exportString: dati,eventName: match )
        }
    }
    
    internal func handleList(tableView: UITableView,indexPath: IndexPath){
          tableView.cellForRow(at: NSIndexPath.init(row: lastRowForSection[0], section: 0) as IndexPath)?.accessoryType = .none
          if let cell = tableView.cellForRow(at: indexPath) {
            cell.textLabel?.textColor = UIColor.black
              lastRowForSection[0] = indexPath.row
              //cell.accessoryType = .checkmark
              if let id = events?[indexPath.row].id {
                  currentEvent = Int(id)
                  self.actionGetAreas(strEvent: String(Int(id)), checkSpinner: true)
              }
          }
    }
    
    // Handle select Mode
    internal func handleModeCheck(tableView: UITableView,indexPath: IndexPath){
        if (indexPath.section == 2){
            for visibleIndex in tableView.indexPathsForVisibleRows! {
                tableView.cellForRow(at: visibleIndex)?.accessoryType = .none
            }
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark
            tableView.tintColor = .systemBlue
            SettingsManager.sharedInstance.saveModeCheck = "ModeCheck\(indexPath.row)"
        }
    }
    
    // Handle select Area
    internal func handleArea(tableView: UITableView,indexPath: IndexPath){
        if (indexPath.section == 3){
            self.actionGetSectors(strArea: self.arrAreas[indexPath.row], checkSpinner: true)
        }
    }

    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if (indexPath.section == 2){
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCell.AccessoryType.none
        }
    }
    
    // Gesture for remove Area or more
    override func tableView(_ tableView: UITableView,
                           trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if (indexPath.section == 3){
            let trash = UIContextualAction(style: .destructive,
                                           title: NSLocalizedString("remove_area", comment: "")) { [weak self] (action, view, completionHandler) in
                self?.removeArea(idx: indexPath.row)
                                            completionHandler(true)
            }
            trash.backgroundColor = .systemRed
            return UISwipeActionsConfiguration(actions: [trash])
        }else{
            let swipeAction = UISwipeActionsConfiguration(actions: [])
            return swipeAction
        }
    }
    
    // Header Sections
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let frame = tableView.frame
        let button = UIButton(frame: CGRect(x: frame.size.width - 100, y: 10, width: 100, height: 20))
        button.tag = section
        button.setTitle(NSLocalizedString("validation", comment: ""), for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.addTarget(self,action:#selector(confirmAllData),for:.touchUpInside)
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        let lblTitle = UILabel(frame: CGRect(x: 5, y: 10, width: 150, height: 20))
        if (section == 3){
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
            headerView.addSubview(button)
        }
        lblTitle.text = NSLocalizedString(self.section[section], comment: "")
        lblTitle.textColor = .lightGray
        headerView.addSubview(lblTitle)
        return headerView
    }
    
    // MARK: - Actions
    
    // Function for remove Area
    private func removeArea(idx: Int) {
        let area = self.arrAreas[idx]
        if(self.appDelegate.arrAreasAndSectors.count > 0){
            for (key, value) in self.appDelegate.arrAreasAndSectors{
                print("\(key): \(value)")
                if (key == area){
                    self.appDelegate.arrAreasAndSectors.removeValue(forKey: area)
                }
            }
            DispatchQueue.main.async { [self] in
                let sectionIndex = IndexSet(integer: 3)
                tableView.reloadSections(sectionIndex, with: .none)
            }
        }
    }
    
    // Function to confirm the selected settings
    @objc func confirmAllData()
    {
        if(self.arrAreas.count > 0){
            DataManager.shared.persistentContainer.performBackgroundTask{(context) in
                /*guard let logs = DataManager.shared.fetchLogs(forEvent: self.currentEvent, withContext: context), logs.count > 0 else {
                    DispatchQueue.delay(.milliseconds(300)) { [self] in
                        self.alertErrorLogs()
                    }
                    return
                }*/
                DispatchQueue.delay(.milliseconds(300)) { [self] in
                    self.alertConfirmSettings(event: currentEvent!)
                }
            }
        }else{
            self.alertErrorNoAreas()
        }
    }
    
    // Function for GET areas
    @objc func actionGetAreas(strEvent: String, checkSpinner: Bool) {
        if(checkSpinner){
            self.showSpinner(onView: self.navigationController?.view)
        }
        APIManager.shared.getAreas(event: strEvent, onSuccess: { json  in
            DispatchQueue.main.async { [self] in
                self.removeSpinner()
                if(json.count > 0){
                    arrAreas = []
                    arrAreas = json
                    if  UserDefaults.standard.integer(forKey: "event") != currentEvent  {
                        self.appDelegate.arrAreasAndSectors = [String : [String]]()
                    }
                    let indexPath = NSIndexPath(row: 0, section: 3)
                    let sectionIndex = IndexSet(integer: 3)
                    tableView.reloadSections(sectionIndex, with: .none)
                    tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                }else{
                    DispatchQueue.main.async { [self] in
                        self.alertErrorNoAreas()
                        self.arrAreas = []
                        let sectionIndex = IndexSet(integer: 3)
                        tableView.reloadSections(sectionIndex, with: .none)
                    }
                }
            }
        }, onFailure: { strError  in
            RetryManager.sharedInstance.startRetry(retryCount: 5) { (result) in
                if(result){
                    self.actionGetAreas(strEvent: strEvent, checkSpinner: checkSpinner)
                }else{
                    DispatchQueue.main.async { [self] in
                        print(strError)
                        self.removeSpinner()
                        self.alertErrorGeneral()
                    }
                }
            }
        })
    }
    
    // Function for GET sectors
    @objc func actionGetSectors(strArea: String, checkSpinner: Bool)
    {
        if(checkSpinner){
            self.showSpinner(onView: self.navigationController?.view)
        }
        APIManager.shared.getSectors(event: String(currentEvent!), area: strArea, onSuccess: { json  in
            DispatchQueue.main.async { [self] in
                self.removeSpinner()
                if(json.count > 0){
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc : SectorsViewController = (storyboard.instantiateViewController(withIdentifier: "sectorsVC") as? SectorsViewController)!
                    vc.arrSectorsLocal = json
                    vc.strArea = strArea
                    if(self.appDelegate.arrAreasAndSectors.count > 0){
                        if let p = self.appDelegate.arrAreasAndSectors[strArea] {
                            print(p)
                        }else{
                            self.appDelegate.arrAreasAndSectors[strArea] = json
                        }
                    }else{
                        self.appDelegate.arrAreasAndSectors[strArea] = json
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    DispatchQueue.main.async { [self] in
                        self.appDelegate.arrAreasAndSectors[strArea] = []
                    }
                }
                DispatchQueue.main.async { [self] in
                    let sectionIndex = IndexSet(integer: 3)
                    tableView.reloadSections(sectionIndex, with: .none)
                }
            }
        }, onFailure: { strError  in
            RetryManager.sharedInstance.startRetry(retryCount: 3) { (result) in
                if(result){
                    self.actionGetSectors(strArea: strArea, checkSpinner: checkSpinner)
                }else{
                    DispatchQueue.main.async { [self] in
                        print(strError)
                        self.removeSpinner()
                        self.alertErrorGeneral()
                    }
                }
            }
        })
    }
    
    // Function refresh Events
    @objc func refreshEvents(_ sender: AnyObject) {
        DispatchQueue.delay(.milliseconds(500)) {
            APIManager.shared.loadEvents(onSuccess: { checkEvent  in
                DispatchQueue.main.async {
                    print("loadEvents \(checkEvent)")
                    self.setEvents()
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                }
            },onFailure: { strError  in
                DispatchQueue.main.async {
                    print(strError)
                    print("loadEvents Error")
                    self.refreshControl?.endRefreshing()
                    self.alertErrorEvents()
                }
            })
        }
    }
    
    // Function logout
    @IBAction func actionLogout(_ sender: UIButton) {
        self.appDelegate.stopBackgrounProcs()
        self.appDelegate.arrAreasAndSectors.removeAll()
        self.appDelegate.arrAreasAndSectorsFinal.removeAll()
        
        DataManager.shared.persistentContainer.performBackgroundTask{(context) in
            DataManager.shared.removeAllSteward(withContext: context)
            DataManager.shared.removeAllLog(withContext: context)
            DataManager.shared.removeAllEvent(withContext: context)
        }
        
        self.appDelegate.resetUserDefaults()
        self.appDelegate.setFirstOpenAppAndSetModeCamera()
        self.closeView()
        
        DispatchQueue.delay(.milliseconds(300)) {
            self.appDelegate.loadLoginView()
        }
    }
    
    //change Event, set variable true
    func setChangeEvent(){
        self.changeEvent = true
    }
    
    //Fetch events from CoreData
    func setEvents() {
        events = DataManager.shared.getEvents()
    }

}
