//
//  ScannerViewController.swift
//  CFScanner
//
//  Created by Alessandro de Peppo on 20/06/2019.
//  Copyright © 2019 Bluenet. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation


class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate, DetailViewCameraDelegate {

    @IBOutlet weak var scannerView: ScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    @IBOutlet weak var scanButton: UIButton! {
        didSet {
            scanButton.setTitle("Interrompi Scansione", for: .normal)
        }
    }
    
    @IBOutlet weak var detailView: DetailView!/*{
        didSet {
            detailView.delegate = self
        }
    }*/
    
    @IBOutlet weak var detailViewCamera: DetailViewCamera!{
        didSet {
            detailViewCamera.delegate = self
        }
    }
    
    @IBOutlet  weak var countLeft: UILabel?
    @IBOutlet  weak var countChecked: UILabel?
    @IBOutlet  weak var eventLabel: UILabel?
    @IBOutlet  weak var textFieldCode: UITextField?
    @IBOutlet  weak var lblSettings: UILabel?
    @IBOutlet  weak var btnInfo: UIButton?

    var uid : String?
    var currentUser: Steward?
    var event : Int?
    var activeInfrared : Bool = false
    var textField: UITextField?
    internal var modeType : ModeType = .checkIn
    private var dwi : DispatchWorkItem?
    var stringCode : String? = ""
    var modeCheck : String? = ""
    var events : [Event]?
    var stringInfo : String? = ""

    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

        setEvents()
        let image = UIImage(named: "controls")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action:#selector(openSettings))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem:.compose, target: self, action:#selector(openList))
        self.hideKeyboardWhenTappedAround()
        textFieldCode?.delegate = self
        let textFieldRecognizer = UITapGestureRecognizer()
        textFieldRecognizer.addTarget(self, action: #selector(tappedTextFieldCode(_:)))
        textFieldCode?.addGestureRecognizer(textFieldRecognizer)
        textFieldCode?.isHidden = true
        textFieldCode?.isUserInteractionEnabled = false
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureToDrag(gesture:)))
        textFieldCode?.addGestureRecognizer(gesture)
        self.detailViewCamera.isHidden = true

        
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Set Observer in order to detect changes or rescues in the entity
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(managedObjectContextObjectsDidChange(notification:)), name: .NSManagedObjectContextObjectsDidChange, object: nil)
        notificationCenter.addObserver(self, selector: #selector(managedObjectContextObjectsDidChange(notification:)), name: .NSManagedObjectContextDidSave, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(closeActivityController), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openactivity), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        Torch().turnOnTorch()
        
        setViewTitle()
        
        let savedEvent = UserDefaults.standard.integer(forKey: "event")
        if (savedEvent > 0)  {
            if event != savedEvent {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.stopBackgrounProcs()
                appDelegate.startBackgroundProcs(forEvent: savedEvent)
            }
            event = savedEvent
            refreshCount(forEvent: savedEvent)
        } else {
            event = nil
            self.eventLabel?.text = "Incontro"
            refreshCount(forEvent: 0)
        }
        
        if !scannerView.isRunning {
            scannerView.startScanning()
            let buttonTitle = scannerView.isRunning ? "Interrompi Scansione" : "Nuova Scansione"
            scanButton.setTitle(buttonTitle, for: .normal)
        }
        
        if let e = self.event, let rowId = events?.lastIndex(where:{ $0.id == e })  {
            eventLabel?.text = events?[rowId].name
        }
        
        self.modeCheck = SettingsManager.sharedInstance.saveModeCheck
        if(self.modeCheck == ModeCheck.checkInfrared.rawValue){
            detailViewCamera.isHidden = true
            scannerView.isHidden = true
            scanButton.isHidden = true
            textFieldCode?.isHidden = false
            textFieldCode?.isUserInteractionEnabled = true
            self.resetTextCode()
            let textFieldRecognizer = UITapGestureRecognizer()
            textFieldRecognizer.addTarget(self, action: #selector(resetTextCode))
            textFieldCode?.addGestureRecognizer(textFieldRecognizer)
        }else{
            setCameraTimer()
            detailView.isHidden = true
            scannerView.isHidden = false
            scanButton.isHidden = false
            textFieldCode?.isHidden = true
            textFieldCode?.isUserInteractionEnabled = false
        }
        
        if  UserDefaults.standard.integer(forKey: "event") == event  {
            self.appDelegate.arrAreasAndSectors = self.appDelegate.arrAreasAndSectorsFinal
            self.checkSavedSetting()
        }else{
            self.checkSavedSetting()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        Torch().turnOffTorch()
        removeDispatchWorkItem()
        self.activeInfrared = false
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    
    //Function check saved settings
    func checkSavedSetting(){
        if(self.appDelegate.arrAreasAndSectors.count > 0){
            var stringAreas = ""
            var stringSectors = ""
            print(self.appDelegate.arrAreasAndSectors)
            for (key, value) in appDelegate.arrAreasAndSectors{
                if(stringAreas.isEmpty){
                    stringAreas = key
                }else{
                    stringAreas = stringAreas + "," + key
                }
                if(stringSectors.isEmpty){
                    stringSectors = value.joined(separator: ",")
                }else{
                    stringSectors = stringSectors + "," + value.joined(separator: ",")
                }
                self.stringInfo = stringAreas + " " + stringSectors
            }
            self.lblSettings?.text = self.stringInfo
            self.btnInfo?.isHidden = false
        }else{
            self.stringInfo = ""
            self.lblSettings?.text = self.stringInfo
            self.btnInfo?.isHidden = true
        }
    }
    
    // MARK: TextField method

    /*func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        stringCode = stringCode! + string
        print("aaaaah")
        print(textField.text)
        print(string)

        if string == "" && textField.text == " "{
            return false
        }
        let trim = stringCode?.trimString()
        if trim == textField.text{
            DispatchQueue.delay(.milliseconds(100)) { [self] in
                self.scanningSucceededWithCode(textField.text)
                self.stringCode = ""
            }
            return true
        }
        return true

    }*/
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldCode!.endEditing(true)
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if(!activeInfrared){
            if textField.text == "" || textField.text == " "{
                stringCode = ""
            }else{
                stringCode = stringCode! + textField.text!
                let trim = stringCode?.trimString()
                if trim == textField.text{
                    DispatchQueue.delay(.milliseconds(100)) { [self] in
                        self.scanningSucceededWithCode(textField.text)
                        self.stringCode = ""
                    }
                }
            }
        }
    }
    
    
    @objc func tappedTextFieldCode(_ sender: UITapGestureRecognizer) {
        self.textFieldCode?.text = ""
        self.textFieldCode?.becomeFirstResponder()
    }

    @objc func panGestureToDrag(gesture: UIPanGestureRecognizer){
        self.textFieldCode?.text = ""
        self.textFieldCode?.becomeFirstResponder()
    }

    @IBAction func scanButtonAction(_ sender: UIButton) {
        if (scannerView.isRunning) {
            scannerView.stopScanning()
            removeDispatchWorkItem()
        } else {
            scannerView.startScanning()
            setCameraTimer()
        }
        let buttonTitle = scannerView.isRunning ? "Interrompi Scansione" : "Nuova Scansione"
        sender.setTitle(buttonTitle, for: .normal)
    }
    
    @IBAction func torchButtonAction(_ sender: Any) {
        let _ = Torch().torchAction()
    }
    
    func refreshCount(forEvent event :Int) {
        let checkLeft = modeType == .checkIn ? [CheckIn.noCheck.rawValue] : [CheckIn.local.rawValue,CheckIn.external.rawValue]
        let totalCheck = modeType == .checkIn ? [CheckIn.local.rawValue,CheckIn.external.rawValue] : [CheckOut.local.rawValue,CheckOut.external.rawValue]
        let localCheck = modeType == .checkIn ? [CheckIn.local.rawValue] : [CheckOut.local.rawValue]
        
        if let left = DataManager.shared.countAll(forEvent: event) {
            countLeft?.text = "\(left)"
        }
        if let checked = DataManager.shared.countChecked(withCheck: totalCheck, forEvent: event, ModeType: modeType),let checkedLocaly = DataManager.shared.countChecked(withCheck: localCheck, forEvent: event, ModeType: modeType) {
            countChecked?.text = "\(checkedLocaly) (\(checked))"
        }
    }
    
    //Fetch events from CoreData
    func setEvents() {
        events = DataManager.shared.getEvents()
    }
    
    @objc func openSettings() {
        activeInfrared = true
        //self.textFieldCode?.resignFirstResponder()
        DispatchQueue.delay(.milliseconds(300)) {
            self.alertRequiredPassword()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "settings")  {
            self.detailViewCamera.isHidden = true
            resetView()
            if let settingsController = segue.destination as? SettingsViewController {
                settingsController.events = events
                settingsController.currentEvent = event
            }
        } else if (segue.identifier == "showList") {
            navigationItem.title = ""
            if let stewardListController = segue.destination as? StewardListViewController, let event = event {
                stewardListController.event = event
            }
        }
    }
    
    func configurationTextField(textField: UITextField!) {
        if (textField) != nil {
            self.textField = textField!
            self.textField?.placeholder = "Codice";
        }
    }
    
    @objc func openList() {
        guard let event = event, event > 0 else {
            return
        }
        DispatchQueue.delay(.milliseconds(300)) {
            self.performSegue(withIdentifier: "showList", sender: nil)
        }

    }
    
    private func setViewTitle() {
        modeType = ModeType(rawValue: UserDefaults.standard.integer(forKey: "Mode")) ?? .checkIn
        //navigationItem.title = "Steward " + NSLocalizedString("Mode\(modeType.rawValue)", comment: "")
        navigationItem.title = "BAM STADIUM"
    }
    
    private func resetView() {
        /*self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem:.compose, target: self, action:#selector(self.openList))
        self.navigationItem.leftBarButtonItem?.tintColor = .systemBlue*/
        //self.detailView?.isHidden = true
        self.currentUser = nil
    }
    
}


extension ScannerViewController: ScannerViewDelegate {
    func scanningDidStop() {
        let buttonTitle = scannerView.isRunning ? "Interrompi Scansione" : "Nuova Scansione"
        scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func scanningDidFail() {
        presentAlert(withTitle: "Errore", message: "Scansione fallita. Prova ancora")
        delayCameraTimer()
    }
    
    func scanningSucceededWithCode(_ str: String?) {
        delayCameraTimer()
        guard let event = self.event else {
            self.resetTextCode()
            presentAlert(withTitle: "Errore", message: "Evento non selezionato")
            return
        }
        
        guard let userCF = str else {
            self.resetTextCode()
            presentAlert(withTitle: "Errore", message: "Impossible recuperare i dati dal Codice")
            return
        }
        
        guard let steward = DataManager.shared.getSteward(userCF, forEvent: event) else {
            self.resetTextCode()
            self.setColorRed()
            
            self.detailView.cognome.text = ""
            self.detailView.nome.text = ""
            self.detailView.seat.text = ""
            self.detailView.dob.text = ""
            self.detailView.row.text = ""
            
            self.detailView.cf.text = userCF
            self.detailView.checked?.text = "Codice \(userCF) non presente"
            self.detailView.isHidden = false
            //presentAlert(withTitle: "Errore", message: "Codice \(userCF) non presente")
            return
        }
        
        /*if steward.valid == false {
            self.resetTextCode()
            self.detailView.checked.textColor = .white
            self.detailView.backgroundView.backgroundColor = .red
            self.detailView.checked?.text = "Codice \(userCF) annullato"
            //presentAlert(withTitle: "Errore", message: "Codice \(userCF) annullato")
            return
        }*/

       if modeType == .checkOut,steward.checkTime == nil {
           self.resetTextCode()
           presentAlert(withTitle: "Errore", message: "Steward non ha effettuato il Check-in!")
           return
       }
        
        self.detailView.confButton?.isHidden = false
        
        if steward.valid == false {
            self.resetTextCode()
            self.setColorRed()
            self.detailView.checked?.text = "Codice \(userCF) annullato"
            //presentAlert(withTitle: "Errore", message: "Codice \(userCF) annullato")
        }else if let checkTime = modeType == .checkIn ? steward.checkTime : steward.checkOutTime {
            self.resetTextCode()
            let dateAsString = "\(checkTime)"
            print(dateAsString)
            print("dateAsString")

            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
            let date = df.date(from: dateAsString)
            df.dateFormat = "dd-MM-yyyy hh:mm:ss"
            df.timeZone = TimeZone.current
            df.locale = Locale.current
            df.dateStyle = .medium
            df.timeStyle = .medium
            let checkTimeFinal = df.string(from: date!)
            self.detailView.checked?.text = "Codice già controllato il " + checkTimeFinal
            //self.detailView.checked?.text = "Codice già controllato alle \(DateFormatter.iso8601UTC.string(from: checkTime))"
            self.setColorYellow()
            self.detailView.checked?.isHidden = false
            self.detailView.confButton?.isHidden = true
        } else {
            self.currentUser = steward
            self.detailView.checked?.isHidden = true
        }
        
        self.detailView.cognome?.text = steward.familyName
        self.detailView.nome?.text = steward.givenName
        self.detailView.cf?.text = steward.cf
        
        if let dob = steward.dob {
            self.detailView.dob?.text = DateFormatter.df.string(from: dob)
            self.detailView.dob?.isHidden = false
        } else {
            self.detailView.dob?.isHidden = true
        }
        
        self.detailView.row?.text = steward.row
        self.detailView.seat?.text = steward.seat

        switch steward.extra {
            case "warn":
            self.detailView.backgroundView.backgroundColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
            //self.detailView?.backgroundColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
            case "cu":
            self.detailView.backgroundView.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            //self.detailView?.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            default:
            break
            //self.detailView.backgroundView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            //self.detailView?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }

        setDetailView(steward: steward)
        
        /*self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem:.cancel, target: self, action:#selector(backButton))
        self.navigationItem.leftBarButtonItem?.tintColor = .systemRed*/
        
        self.detailView?.isHidden = false
        self.confirmButton()
    }
    
    func scanningSucceededWithCodeCamera(_ str: String?) {
        self.detailViewCamera.isHidden = false
        self.detailViewCamera.backgroundView.backgroundColor = .white
        
        delayCameraTimer()
        guard let event = self.event else {
            presentAlert(withTitle: "Errore", message: "Evento non selezionato")
            return
        }
        
        guard let userCF = str else {
            presentAlert(withTitle: "Errore", message: "Impossible recuperare i dati dal Codice")
            return
        }
        
        guard let steward = DataManager.shared.getSteward(userCF, forEvent: event) else {
            self.setColorRed()
            
            self.detailViewCamera.cognome.text = ""
            self.detailViewCamera.nome.text = ""
            self.detailViewCamera.seat.text = ""
            self.detailViewCamera.dob.text = ""
            self.detailViewCamera.row.text = ""
            
            self.detailViewCamera.cf.text = userCF
            self.detailViewCamera.checked?.text = "Codice \(userCF) non presente"
            self.confirmTicketButton()
            //presentAlert(withTitle: "Errore", message: "Codice \(userCF) non presente")
            return
        }
        
        /*if steward.valid == false {
            self.resetTextCode()
            self.detailView.checked.textColor = .white
            self.detailView.backgroundView.backgroundColor = .red
            self.detailView.checked?.text = "Codice \(userCF) annullato"
            //presentAlert(withTitle: "Errore", message: "Codice \(userCF) annullato")
            return
        }*/

       if modeType == .checkOut,steward.checkTime == nil {
           presentAlert(withTitle: "Errore", message: "Steward non ha effettuato il Check-in!")
           return
       }
        
        self.detailViewCamera.confButton?.isHidden = false
        
        if steward.valid == false {
            self.setColorRed()
            self.detailViewCamera.checked?.text = "Codice \(userCF) annullato"
            //presentAlert(withTitle: "Errore", message: "Codice \(userCF) annullato")
        }else if let checkTime = modeType == .checkIn ? steward.checkTime : steward.checkOutTime {
            let dateAsString = "\(checkTime)"
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
            let date = df.date(from: dateAsString)
            df.dateFormat = "dd-MM-yyyy hh:mm:ss"
            df.timeZone = TimeZone.current
            df.locale = Locale.current
            df.dateStyle = .medium
            df.timeStyle = .medium
            let checkTimeFinal = df.string(from: date!)
            self.detailViewCamera.checked?.text = "Codice già controllato il " + checkTimeFinal
            //self.detailViewCamera.checked?.text = "Codice già controllato alle \(DateFormatter.iso8601UTC.string(from: checkTime))"

            self.setColorYellow()
            self.detailViewCamera.checked?.isHidden = false
            self.detailViewCamera.confButton?.isHidden = true
        } else {
            self.currentUser = steward
            self.detailViewCamera.checked?.isHidden = true
        }
        
        self.detailViewCamera.cognome?.text = steward.familyName
        self.detailViewCamera.nome?.text = steward.givenName
        self.detailViewCamera.cf?.text = steward.cf
        
        if let dob = steward.dob {
            self.detailViewCamera.dob?.text = DateFormatter.df.string(from: dob)
            self.detailViewCamera.dob?.isHidden = false
        } else {
            self.detailViewCamera.dob?.isHidden = true
        }
        
        self.detailViewCamera.row?.text = steward.row
        self.detailViewCamera.seat?.text = steward.seat

        switch steward.extra {
            case "warn":
            self.detailViewCamera.backgroundColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
            //self.detailView?.backgroundColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
            case "cu":
            self.detailViewCamera.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            //self.detailView?.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            default:
            break
            //self.detailView.backgroundView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            //self.detailView?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }

        setDetailViewCamera(steward: steward)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem:.cancel, target: self, action:#selector(backButton))
        self.navigationItem.leftBarButtonItem?.tintColor = .systemRed
        
        self.detailViewCamera?.isHidden = false
        //self.confirmButton()
    }
    
    @objc func backButton() {
        scannerView.startScanning()
        let buttonTitle = "Interrompi Scansione"
        scanButton.setTitle(buttonTitle, for: .normal)
        self.currentUser = nil
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem:.compose, target: self, action:#selector(openList))
        self.navigationItem.leftBarButtonItem?.tintColor = .systemBlue
        self.detailViewCamera.isHidden = true

    }
    
    @objc func confirmTicketButton() {
        scannerView.startScanning()
        let buttonTitle = "Interrompi Scansione"
        scanButton.setTitle(buttonTitle, for: .normal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem:.cancel, target: self, action:#selector(backButton))
        self.navigationItem.leftBarButtonItem?.tintColor = .systemRed
        //self.detailViewCamera.isHidden = true
    }
    
    @objc func resetTextCode() {
        DispatchQueue.main.async {
            self.textFieldCode?.text = ""
            self.textFieldCode?.becomeFirstResponder()
        }
    }
    
    func setColorGreen(){
        if(self.modeCheck == ModeCheck.checkInfrared.rawValue){
            self.detailView.lblDate.textColor = .white
            self.detailView.lblCognome.textColor = .white
            self.detailView.lblNome.textColor = .white
            self.detailView.lblFila.textColor = .white
            self.detailView.lblPosto.textColor = .white
            self.detailView.backgroundView.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }else{
            self.detailViewCamera.lblDate.textColor = .white
            self.detailViewCamera.lblCognome.textColor = .white
            self.detailViewCamera.lblNome.textColor = .white
            self.detailViewCamera.lblFila.textColor = .white
            self.detailViewCamera.lblPosto.textColor = .white
            self.detailViewCamera.backgroundView.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        }
    }
    
    func setColorYellow(){
        if(self.modeCheck == ModeCheck.checkInfrared.rawValue){
            self.detailView.lblDate.textColor = .gray
            self.detailView.lblCognome.textColor = .gray
            self.detailView.lblNome.textColor = .gray
            self.detailView.lblFila.textColor = .gray
            self.detailView.lblPosto.textColor = .gray
            self.detailView.backgroundView.backgroundColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
            self.detailView.checked.textColor = .red
        }else{
            self.detailViewCamera.lblDate.textColor = .gray
            self.detailViewCamera.lblCognome.textColor = .gray
            self.detailViewCamera.lblNome.textColor = .gray
            self.detailViewCamera.lblFila.textColor = .gray
            self.detailViewCamera.lblPosto.textColor = .gray
            self.detailViewCamera.backgroundView.backgroundColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
            self.detailViewCamera.checked.textColor = .red
        }
    }
    
    func setColorRed(){
        if(self.modeCheck == ModeCheck.checkInfrared.rawValue){
            self.detailView.lblDate.textColor = .white
            self.detailView.lblCognome.textColor = .white
            self.detailView.lblNome.textColor = .white
            self.detailView.lblFila.textColor = .white
            self.detailView.lblPosto.textColor = .white
            self.detailView.backgroundView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.detailView.checked.textColor = .white
        }else{
            self.detailViewCamera.lblDate.textColor = .white
            self.detailViewCamera.lblCognome.textColor = .white
            self.detailViewCamera.lblNome.textColor = .white
            self.detailViewCamera.lblFila.textColor = .white
            self.detailViewCamera.lblPosto.textColor = .white
            self.detailViewCamera.backgroundView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            self.detailViewCamera.checked.textColor = .white
        }
    }
    
    @IBAction func infoButtonAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : InfoAreaSectorViewController = (storyboard.instantiateViewController(withIdentifier: "areasandsectorsVC") as? InfoAreaSectorViewController)!
        vc.modalPresentationStyle = .fullScreen
        vc.arrAreasAndSectorsLocal = self.appDelegate.arrAreasAndSectors
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
}

extension ScannerViewController {
    
       // MARK: - Core Data Objects DidChange
     
       @objc func managedObjectContextObjectsDidChange(notification: NSNotification) {
     
           // Get all info about the context that generated the notification
           guard let userInfo = notification.userInfo else { return }
    
           // Do something when there are inserts value in Steward Entity
           if let inserts = userInfo[NSInsertedObjectsKey] as? Set<Steward>, inserts.count > 0 {

                // change UILabel value in the main thread
               DispatchQueue.delay(.milliseconds(100)) {
                    self.refreshCount(forEvent: self.event!)
                }
            }

           
            // Do something when there are updates value in Steward Entity
            if let updates = userInfo[NSUpdatedObjectsKey] as? Set<Steward>, updates.count > 0 {
                    // change UILabel value in the main thread
                DispatchQueue.delay(.milliseconds(100)) {
                        self.refreshCount(forEvent: self.event!)
                    }
            }
        
            // Do something when there are inserts value in Event Entity
            if let inserts = userInfo[NSInsertedObjectsKey] as? Set<Event>, inserts.count > 0 {

                 // change events array in the main thread
                DispatchQueue.delay(.milliseconds(100)) {
                     self.setEvents()
                 }
             }
         
        }
}

extension DateFormatter {
    static let iso8601UTC: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    static let isodate8601UTC: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    static let dtf: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale.current
        return formatter
    }()
    
    static let dtfCsv: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyy_HHmmss"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale.current
        return formatter
    }()
    
    static let df: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone.current
        formatter.locale = Locale.current
        return formatter
    }()
    
    static let dfTS: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter
    }()
    
    static let dfDOB: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter
    }()

}

extension ScannerViewController {
    @objc func closeActivityController()  {
        Torch().turnOffTorch()
    }

    //view should reload the data.
    @objc func openactivity()  {
        Torch().turnOnTorch()
    }
}

// MARK: - Camera Timer methods
extension ScannerViewController {
    
    private func setCameraTimer() {
        setDispatchWorkItem()
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 30.0, execute: dwi!)
    }
    
    private func delayCameraTimer() {
        removeDispatchWorkItem()
        setDispatchWorkItem()
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 30.0, execute: dwi!)
    }
    
    private func setDispatchWorkItem() {
        dwi = DispatchWorkItem {
            DispatchQueue.delay(.milliseconds(300)) {
               self.scannerView.stopScanning()
               self.scanButton.setTitle("Nuova Scansione", for: .normal)
           }
        }
    }
    
    private func removeDispatchWorkItem() {
        dwi?.cancel()
    }
}

extension ScannerViewController: DetailViewDelegate {
    func confirmButton() {
        self.resetTextCode()
        if let user = self.currentUser, let event = self.event {
            let checkTime = Date.init()
            if(modeType == .checkIn){
                user.checkTime = checkTime
                user.checkIn = CheckIn.local.rawValue
            } else {
                user.checkOutTime = checkTime
                user.checkOut = CheckOut.local.rawValue
            }
            setDataLog(user: user, checkTime: checkTime, event: event)
            
            DataManager.shared.update(user)

            refreshCount(forEvent: event)
            self.setColorGreen()

        }
        scannerView.startScanning()
        let buttonTitle = "Interrompi Scansione"
        scanButton.setTitle(buttonTitle, for: .normal)
        resetView()
        if(self.modeCheck == ModeCheck.checkCamera.rawValue){
            self.backButton()
        }
        
    }
    
    
}
extension DispatchQueue {

    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }

}
