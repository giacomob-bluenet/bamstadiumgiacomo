//
//  SectorsViewController.swift
//  Steward Check-in
//
//  Created by Nicola on 27/10/21.
//  Copyright © 2021 KM, Abhilash. All rights reserved.
//

import Foundation
import UIKit

class SectorsViewController: UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    var arrSectorsLocal: [String] = []
    var strArea : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = strArea
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("remove_all", comment: ""), style: .done, target: self, action: #selector(removeFilters))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSectorsLocal.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.textLabel?.text = self.arrSectorsLocal[indexPath.row]
        cell.textLabel?.textColor = .black
        let sector = self.arrSectorsLocal[indexPath.row]
        let contains = self.appDelegate.arrAreasAndSectors[strArea]!.contains(where: {$0 == sector})
        if contains {
            cell.accessoryType = .checkmark
            cell.tintColor = .systemBlue
        }else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sector = self.arrSectorsLocal[indexPath.row]
        if let index = self.appDelegate.arrAreasAndSectors[strArea]!.firstIndex(of: sector) {
            self.appDelegate.arrAreasAndSectors[strArea]!.remove(at: index)
        }else{
            self.appDelegate.arrAreasAndSectors[strArea]!.append(sector)
        }
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
    
    //Function to remove all selected filters
    @objc func removeFilters(){
        for i in 0...self.arrSectorsLocal.count-1 {
            let sector = self.arrSectorsLocal[i]
            if let index = self.appDelegate.arrAreasAndSectors[strArea]!.firstIndex(of: sector) {
                self.appDelegate.arrAreasAndSectors[strArea]!.remove(at: index)
            }
        }
        self.tableView.reloadData()
    }



}
